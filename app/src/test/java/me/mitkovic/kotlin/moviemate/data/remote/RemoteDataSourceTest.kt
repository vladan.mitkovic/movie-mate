package me.mitkovic.kotlin.moviemate.data.remote

import kotlinx.coroutines.test.runTest
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.testing.data.remote.TestRemoteDataSourceImpl
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class RemoteDataSourceTest {

    private val remoteDataSource: RemoteDataSource = TestRemoteDataSourceImpl

    @Before
    fun setUp() {
        // Set up data for testing
        val testMovies =
            listOf(
                Constants.MOVIE1,
                Constants.MOVIE2,
            )
        val testMovieDetails = testMovies.associateBy { it.id }
        val testSimilarMovies = mapOf(1 to listOf(testMovies[1]))
        val testSearchResults = listOf(testMovies[0])

        TestRemoteDataSourceImpl.setPopularMovies(testMovies)
        TestRemoteDataSourceImpl.setMovieDetails(testMovieDetails)
        TestRemoteDataSourceImpl.setSimilarMovies(testSimilarMovies)
        TestRemoteDataSourceImpl.setSearchResults(testSearchResults)
    }

    @Test
    fun `getPopularMovies returns expected data`() =
        runTest {
            val response = remoteDataSource.getPopularMovies(1)
            Assert.assertTrue(response.isSuccessful)
            Assert.assertEquals(2, response.body()?.results?.size)
        }

    @Test
    fun `getMovieDetails returns expected data`() =
        runTest {
            val response = remoteDataSource.getMovieDetails(1)
            Assert.assertTrue(response.isSuccessful)
            Assert.assertEquals("Movie 1", response.body()?.title)
        }

    @Test
    fun `getMovieDetails returns error when movie not found`() =
        runTest {
            val response = remoteDataSource.getMovieDetails(999)
            Assert.assertFalse(response.isSuccessful)
            Assert.assertEquals(404, response.code())
            Assert.assertEquals("{\"message\":\"Movie not found\"}", response.errorBody()?.string())
        }

    @Test
    fun `getSimilarMovies returns expected data`() =
        runTest {
            val response = remoteDataSource.getSimilarMovies(1)
            Assert.assertTrue(response.isSuccessful)
            Assert.assertEquals(1, response.body()?.results?.size)
            Assert.assertEquals(
                "Movie 2",
                response
                    .body()
                    ?.results
                    ?.get(0)
                    ?.title,
            )
        }

    @Test
    fun `searchMovie returns expected data`() =
        runTest {
            val response = remoteDataSource.searchMovie("Movie 1", 1)
            Assert.assertTrue(response.isSuccessful)
            Assert.assertEquals(1, response.body()?.results?.size)
            Assert.assertEquals(
                "Movie 1",
                response
                    .body()
                    ?.results
                    ?.get(0)
                    ?.title,
            )
        }

    @Test
    fun `searchMovie returns empty list when no matches`() =
        runTest {
            val response = remoteDataSource.searchMovie("Nonexistent Movie", 1)
            Assert.assertTrue(response.isSuccessful)
            Assert.assertEquals(0, response.body()?.results?.size)
        }

    @Test
    fun `getMovieGenres returns expected data`() =
        runTest {
            val testGenres = Constants.GENRES_RESPONSE
            TestRemoteDataSourceImpl.setGenresResponse(testGenres)

            val response = remoteDataSource.getMovieGenres()
            Assert.assertTrue(response.isSuccessful)
            Assert.assertEquals(testGenres.genreResponses.size, response.body()?.genreResponses?.size)
        }

    @Test
    fun `getMovieGenres returns error when no genres available`() =
        runTest {
            TestRemoteDataSourceImpl.setGenresResponse(null)

            val response = remoteDataSource.getMovieGenres()
            Assert.assertFalse(response.isSuccessful)
            Assert.assertEquals(404, response.code())
            Assert.assertEquals("{\"message\":\"Genres not found\"}", response.errorBody()?.string())
        }
}
