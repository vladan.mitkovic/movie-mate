package me.mitkovic.kotlin.moviemate.data.local.database

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import me.mitkovic.kotlin.moviemate.data.local.database.genres.GenresDataSource
import me.mitkovic.kotlin.moviemate.data.model.GenreResponse
import me.mitkovic.kotlin.moviemate.testing.data.local.database.TestGenresDataSourceImpl
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class GenresDataSourceTest {

    private val genresDataSource: GenresDataSource = TestGenresDataSourceImpl

    @Test
    fun `saveGenres stores genres correctly`() =
        runTest {
            val genres =
                listOf(
                    GenreResponse(id = 1, name = "Action"),
                    GenreResponse(id = 2, name = "Comedy"),
                )
            genresDataSource.saveGenres(genres)

            val savedGenres = genresDataSource.getGenres().first()
            assertEquals(2, savedGenres.size)
            assertTrue(savedGenres.any { it.name == "Action" })
            assertTrue(savedGenres.any { it.name == "Comedy" })
        }

    @Test
    fun `getGenres returns all saved genres`() =
        runTest {
            val genres =
                listOf(
                    GenreResponse(id = 1, name = "Drama"),
                    GenreResponse(id = 2, name = "Horror"),
                )
            genresDataSource.saveGenres(genres)

            val loadedGenres = genresDataSource.getGenres().first()
            assertEquals(2, loadedGenres.size)
            assertTrue(loadedGenres.any { it.name == "Drama" })
            assertTrue(loadedGenres.any { it.name == "Horror" })
        }

    @Test
    fun `getGenreById returns correct genre`() =
        runTest {
            val genres =
                listOf(
                    GenreResponse(id = 1, name = "Adventure"),
                    GenreResponse(id = 2, name = "Thriller"),
                )
            genresDataSource.saveGenres(genres)

            val genre = genresDataSource.getGenreById(1)
            assertEquals("Adventure", genre?.name)

            val missingGenre = genresDataSource.getGenreById(3)
            assertEquals(null, missingGenre)
        }
}
