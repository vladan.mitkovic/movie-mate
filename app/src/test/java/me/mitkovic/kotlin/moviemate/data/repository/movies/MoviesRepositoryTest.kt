package me.mitkovic.kotlin.moviemate.data.repository.movies

import androidx.paging.testing.asSnapshot
import app.cash.turbine.test
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.testing.data.repository.movies.TestMoviesRepositoryImpl
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class MoviesRepositoryTest {

    private val testDispatcher = StandardTestDispatcher()
    private val moviesRepository: MoviesRepository = TestMoviesRepositoryImpl

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)

        // Set up data for testing
        val testMovies =
            listOf(
                Constants.MOVIE1,
                Constants.MOVIE2,
            )
        val testMovieDetails = testMovies.associateBy { it.id }
        val testSimilarMovies = mapOf(1 to listOf(testMovies[1]))

        TestMoviesRepositoryImpl.setPopularMovies(testMovies)
        TestMoviesRepositoryImpl.setMovieDetails(testMovieDetails)
        TestMoviesRepositoryImpl.setSimilarMovies(testSimilarMovies)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `getPopularMovies returns expected data`() =
        runTest {
            val moviesFlow = moviesRepository.getPopularMovies {}
            val movies = moviesFlow.asSnapshot()

            Assert.assertEquals(2, movies.size)
            Assert.assertEquals(Constants.MOVIE1.title, movies[0].title)
            Assert.assertEquals(Constants.MOVIE2.title, movies[1].title)
        }

    @Test
    fun `getMovieDetails returns expected data`() =
        runTest {
            moviesRepository.getMovieDetails(1).test {
                val loadingState = awaitItem()
                Assert.assertTrue(loadingState is Resource.Loading)

                val result = awaitItem()
                Assert.assertTrue(result is Resource.Success)
                Assert.assertEquals(Constants.MOVIE1.title, (result as Resource.Success).data?.title)

                cancelAndConsumeRemainingEvents()
            }
        }

    @Test
    fun `getSimilarMovies returns expected data`() =
        runTest {
            moviesRepository.getSimilarMovies(1).test {
                val loadingState = awaitItem()
                Assert.assertTrue(loadingState is Resource.Loading)

                val result = awaitItem()
                Assert.assertTrue(result is Resource.Success)
                Assert.assertEquals(1, (result as Resource.Success).data?.results?.size)

                cancelAndConsumeRemainingEvents()
            }
        }

    @Test
    fun `getFavoriteMovies returns no favorites initially`() =
        runTest {
            moviesRepository.getFavoriteMovies().test {
                val loadingState = awaitItem()
                Assert.assertTrue(loadingState is Resource.Loading)

                val result = awaitItem()
                Assert.assertTrue(result is Resource.Success)
                Assert.assertTrue(result.data?.results?.isEmpty() == true)

                cancelAndConsumeRemainingEvents()
            }
        }

    @Test
    fun `getFavoriteMovies returns correct favorite movies`() =
        runTest {
            moviesRepository.updateMovieFavoriteStatus(Constants.MOVIE1.id, true)
            moviesRepository.updateMovieFavoriteStatus(Constants.MOVIE2.id, true)

            moviesRepository.getFavoriteMovies().test {
                val loadingState = awaitItem()
                Assert.assertTrue(loadingState is Resource.Loading)

                val result = awaitItem()
                Assert.assertTrue(result is Resource.Success)
                val favorites = result.data?.results.orEmpty()
                Assert.assertEquals(2, favorites.size)
                Assert.assertTrue(favorites.any { it.id == Constants.MOVIE1.id })
                Assert.assertTrue(favorites.any { it.id == Constants.MOVIE2.id })

                cancelAndConsumeRemainingEvents()
            }
        }

    @Test
    fun `updateMovieFavoriteStatus marks a movie as favorite`() =
        runTest {
            moviesRepository.updateMovieFavoriteStatus(Constants.MOVIE1.id, true)

            val isFavorite = moviesRepository.isFavorite(Constants.MOVIE1.id)
            Assert.assertTrue(isFavorite)

            moviesRepository.getFavoriteMovies().test {
                val loadingState = awaitItem()
                Assert.assertTrue(loadingState is Resource.Loading)

                val result = awaitItem()
                Assert.assertTrue(result is Resource.Success)
                Assert.assertTrue(result.data?.results?.any { it.id == Constants.MOVIE1.id } == true)

                cancelAndConsumeRemainingEvents()
            }
        }

    @Test
    fun `searchMovies returns expected data`() =
        runTest {
            val searchResults = listOf(Constants.MOVIE1)
            TestMoviesRepositoryImpl.setSearchResults(searchResults)

            val searchFlow = moviesRepository.searchMovies("Movie 1")
            val movies = searchFlow.asSnapshot()

            Assert.assertEquals(1, movies.size)
            Assert.assertEquals(Constants.MOVIE1.title, movies[0].title)
        }
}
