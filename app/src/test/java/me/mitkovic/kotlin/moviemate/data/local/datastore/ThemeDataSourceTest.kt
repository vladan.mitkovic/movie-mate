package me.mitkovic.kotlin.moviemate.data.local.datastore

import androidx.appcompat.app.AppCompatDelegate
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import me.mitkovic.kotlin.moviemate.testing.data.local.datastore.TestThemeDataSourceImpl
import org.junit.Assert.assertEquals
import org.junit.Test

class ThemeDataSourceTest {

    private val themeDataSource: ThemeDataSource = TestThemeDataSourceImpl

    @Test
    fun `saveTheme and getTheme work correctly`() =
        runTest {
            themeDataSource.saveTheme(AppCompatDelegate.MODE_NIGHT_NO)
            val theme = themeDataSource.getTheme().first()
            assertEquals(AppCompatDelegate.MODE_NIGHT_NO, theme)

            themeDataSource.saveTheme(AppCompatDelegate.MODE_NIGHT_YES)
            val updatedTheme = themeDataSource.getTheme().first()
            assertEquals(AppCompatDelegate.MODE_NIGHT_YES, updatedTheme)
        }
}
