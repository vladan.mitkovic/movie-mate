package me.mitkovic.kotlin.moviemate.data.local.database

import androidx.paging.PagingSource
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.data.local.database.movies.MoviesDataSource
import me.mitkovic.kotlin.moviemate.testing.data.local.database.TestMoviesDataSourceImpl
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class MoviesDataSourceTest {

    private val moviesDataSource: MoviesDataSource = TestMoviesDataSourceImpl

    @Before
    fun setUp() =
        runBlocking {
            // Clear any existing state in the TestMoviesLocalDataSourceImpl
            moviesDataSource.deleteAllMovies()

            // Set up test Movies in the repository with isFavorite = false
            val testMovies =
                listOf(
                    Constants.MOVIE1.copy(isFavorite = false),
                    Constants.MOVIE2.copy(isFavorite = false),
                )

            // Save Movies into TestMoviesLocalDataSourceImpl
            moviesDataSource.saveMovies(testMovies)
        }

    @Test
    fun `saveMovies stores movies correctly`() =
        runTest {
            moviesDataSource.deleteAllMovies()
            val newMovies = listOf(Constants.MOVIE1.copy(isFavorite = false))
            moviesDataSource.saveMovies(newMovies)

            val savedMovies = moviesDataSource.getAllMovies().first()
            assertEquals(1, savedMovies.size)
            assertEquals(Constants.MOVIE1.title, savedMovies.first().title)
        }

    @Test
    fun `saveMovieDetails updates or adds movie details correctly`() =
        runTest {
            val updatedMovie = Constants.MOVIE1.copy(title = "UpdatedMovie")
            moviesDataSource.saveMovieDetails(updatedMovie)

            val movie = moviesDataSource.getMovieDetails(Constants.MOVIE1.id).first()
            assertEquals("UpdatedMovie", movie?.title)
        }

    @Test
    fun `getMovieDetails returns correct Movie`() =
        runTest {
            val movie1 = moviesDataSource.getMovieDetails(Constants.MOVIE1.id).first()
            assertEquals(Constants.MOVIE1.title, movie1?.title)

            val movie2 = moviesDataSource.getMovieDetails(Constants.MOVIE2.id).first()
            assertEquals(Constants.MOVIE2.title, movie2?.title)
        }

    @Test
    fun `getMovies returns correct PagingSource`() =
        runTest {
            val pagingSource = moviesDataSource.getMovies()
            val pagedData =
                pagingSource.load(
                    PagingSource.LoadParams.Refresh(
                        key = 0,
                        loadSize = 10,
                        placeholdersEnabled = false,
                    ),
                )
            assertTrue(pagedData is PagingSource.LoadResult.Page)
            val page = pagedData as PagingSource.LoadResult.Page
            assertEquals(2, page.data.size)
        }

    @Test
    fun `getAllMovies returns all saved movies`() =
        runTest {
            val allMovies = moviesDataSource.getAllMovies().first()
            assertEquals(2, allMovies.size)
            assertTrue(allMovies.any { it.title == Constants.MOVIE1.title })
            assertTrue(allMovies.any { it.title == Constants.MOVIE2.title })
        }

    @Test
    fun `searchMovies filters and returns correct results`() =
        runTest {
            val result = moviesDataSource.searchMovies(Constants.MOVIE1.title!!).first()
            assertEquals(1, result.size)
            assertEquals(Constants.MOVIE1.title, result.first().title)
        }

    @Test
    fun `deleteAllMovies clears all movies correctly`() =
        runTest {
            assertEquals(2, moviesDataSource.getAllMovies().first().size)

            moviesDataSource.deleteAllMovies()

            val movies = moviesDataSource.getAllMovies().first()
            assertTrue(movies.isEmpty())
        }

    @Test
    fun `updateMovieFavoriteStatus marks a movie as favorite`() =
        runTest {
            moviesDataSource.updateMovieFavoriteStatus(Constants.MOVIE1.id, true)

            val isFavorite = moviesDataSource.getMovieDetails(Constants.MOVIE1.id).first()?.isFavorite
            assertTrue(isFavorite == true)

            val favoriteMovies = moviesDataSource.getFavoriteMovies().first()
            assertTrue(favoriteMovies.any { it.id == Constants.MOVIE1.id })
        }

    @Test
    fun `updateMovieFavoriteStatus unmarks a movie as favorite`() =
        runTest {
            moviesDataSource.updateMovieFavoriteStatus(Constants.MOVIE1.id, true)
            moviesDataSource.updateMovieFavoriteStatus(Constants.MOVIE1.id, false)

            val isFavorite = moviesDataSource.getMovieDetails(Constants.MOVIE1.id).first()?.isFavorite
            assertFalse(isFavorite == true)

            val favoriteMovies = moviesDataSource.getFavoriteMovies().first()
            assertTrue(favoriteMovies.none { it.id == Constants.MOVIE1.id })
        }

    @Test
    fun `getFavoriteMovies returns correct favorite movies`() =
        runTest {
            moviesDataSource.updateMovieFavoriteStatus(Constants.MOVIE1.id, true)
            moviesDataSource.updateMovieFavoriteStatus(Constants.MOVIE2.id, true)

            val favoriteMovies = moviesDataSource.getFavoriteMovies().first()
            assertEquals(2, favoriteMovies.size)
            assertTrue(favoriteMovies.any { it.id == Constants.MOVIE1.id })
            assertTrue(favoriteMovies.any { it.id == Constants.MOVIE2.id })

            moviesDataSource.updateMovieFavoriteStatus(Constants.MOVIE1.id, false)

            val updatedFavorites = moviesDataSource.getFavoriteMovies().first()
            assertEquals(1, updatedFavorites.size)
            assertTrue(updatedFavorites.any { it.id == Constants.MOVIE2.id })
        }

    @Test
    fun `updateMovieFavoriteStatus does not affect other movies`() =
        runTest {
            moviesDataSource.updateMovieFavoriteStatus(Constants.MOVIE1.id, true)

            val favoriteMovies = moviesDataSource.getFavoriteMovies().first()
            assertEquals(1, favoriteMovies.size)
            assertTrue(favoriteMovies.any { it.id == Constants.MOVIE1.id })
            assertFalse(favoriteMovies.any { it.id == Constants.MOVIE2.id })

            val isMovie2Favorite = moviesDataSource.getMovieDetails(Constants.MOVIE2.id).first()?.isFavorite
            assertFalse(isMovie2Favorite == true)
        }
}
