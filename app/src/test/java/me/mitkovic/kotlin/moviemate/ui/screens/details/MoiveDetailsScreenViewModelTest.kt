package me.mitkovic.kotlin.moviemate.ui.screens.details

import app.cash.turbine.test
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.data.model.GenreResponse
import me.mitkovic.kotlin.moviemate.testing.data.repository.genres.TestGenresRepositoryImpl
import me.mitkovic.kotlin.moviemate.testing.data.repository.movies.TestMoviesRepositoryImpl
import me.mitkovic.kotlin.moviemate.testing.ui.screens.details.TestMovieDetailsViewModelProvider
import me.mitkovic.kotlin.moviemate.ui.utils.toMovieDetailsUi
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class MovieDetailsScreenViewModelTest {

    private val testDispatcher = StandardTestDispatcher()
    private lateinit var viewModel: MovieDetailsScreenViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)

        TestMoviesRepositoryImpl.setPopularMovies(emptyList())
        TestMoviesRepositoryImpl.setMovieDetails(emptyMap())
        TestMoviesRepositoryImpl.setSimilarMovies(emptyMap())
        TestGenresRepositoryImpl.setGenres(emptyList())

        viewModel = TestMovieDetailsViewModelProvider.provideMovieDetailsViewModel(Constants.MOVIE1.id)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `movieDetailsUiState emits loading then success`() =
        runTest {
            // Arrange
            val moviePosterUrl = "${Constants.MOVIE_POSTER_URL}${Constants.MOVIE1.moviePosterUrl}"

            val genres =
                listOf(
                    GenreResponse(id = 1, name = "Action"),
                    GenreResponse(id = 2, name = "Comedy"),
                )

            TestGenresRepositoryImpl.setGenres(genres)

            val movieWithGenres = Constants.MOVIE1.copy(genreIds = listOf(1, 2))
            TestMoviesRepositoryImpl.setMovieDetails(mapOf(movieWithGenres.id to movieWithGenres))

            // Act & Assert
            viewModel.movieDetailsUiState.test {
                assertEquals(
                    MovieDetailsUiState(isLoading = false, movieDetails = null, error = null),
                    awaitItem(),
                )
                assertEquals(
                    MovieDetailsUiState(isLoading = true, movieDetails = null, error = null),
                    awaitItem(),
                )
                assertEquals(
                    MovieDetailsUiState(isLoading = false, movieDetails = null, error = null),
                    awaitItem(),
                )
                assertEquals(
                    MovieDetailsUiState(
                        isLoading = false,
                        movieDetails =
                            movieWithGenres.toMovieDetailsUi(
                                TestMoviesRepositoryImpl,
                                moviePosterUrl,
                                "Action, Comedy",
                            ),
                        error = null,
                    ),
                    awaitItem(),
                )

                cancelAndConsumeRemainingEvents()
            }
        }

    @Test
    fun `toggleFavoriteStatus updates favorite state`() =
        runTest {
            // Arrange
            val moviePosterUrl = "${Constants.MOVIE_POSTER_URL}${Constants.MOVIE1.moviePosterUrl}"

            val genres =
                listOf(
                    GenreResponse(id = 1, name = "Action"),
                    GenreResponse(id = 2, name = "Comedy"),
                )

            TestGenresRepositoryImpl.setGenres(genres)

            TestMoviesRepositoryImpl.setPopularMovies(listOf(Constants.MOVIE1))

            // Act
            viewModel.toggleFavoriteStatus(Constants.MOVIE1.id)

            // Assert
            viewModel.movieDetailsUiState.test {
                assertEquals(
                    MovieDetailsUiState(isLoading = false, movieDetails = null, error = null),
                    awaitItem(),
                )
                assertEquals(
                    MovieDetailsUiState(isLoading = true, movieDetails = null, error = null),
                    awaitItem(),
                )
                assertEquals(
                    MovieDetailsUiState(isLoading = false, movieDetails = null, error = null),
                    awaitItem(),
                )
                assertEquals(
                    MovieDetailsUiState(
                        isLoading = false,
                        movieDetails =
                            Constants.MOVIE1
                                .copy(isFavorite = true)
                                .toMovieDetailsUi(TestMoviesRepositoryImpl, moviePosterUrl, "Action, Comedy"),
                        error = null,
                    ),
                    awaitItem(),
                )

                cancelAndConsumeRemainingEvents()
            }
        }

    @Test
    fun `similarMovies emits empty list initially then updates`() =
        runTest {
            // Arrange
            TestMoviesRepositoryImpl.setSimilarMovies(
                mapOf(Constants.MOVIE1.id to listOf(Constants.MOVIE2)),
            )

            // Act & Assert
            viewModel.movieDetailsUiState.test {
                assertEquals(
                    MovieDetailsUiState(
                        isLoading = false,
                        movieDetails = null,
                        similarMovies = SimilarMovies(emptyList()),
                        error = null,
                    ),
                    awaitItem(),
                )
                assertEquals(
                    MovieDetailsUiState(
                        isLoading = true,
                        movieDetails = null,
                        similarMovies = SimilarMovies(emptyList()),
                        error = null,
                    ),
                    awaitItem(),
                )
                assertEquals(
                    MovieDetailsUiState(
                        isLoading = false,
                        movieDetails = null,
                        similarMovies = SimilarMovies(emptyList()),
                        error = null,
                    ),
                    awaitItem(),
                )
                assertEquals(
                    MovieDetailsUiState(
                        isLoading = false,
                        movieDetails = null,
                        similarMovies =
                            SimilarMovies(
                                listOf(
                                    Constants.MOVIE2.copy(
                                        moviePosterUrl = "${Constants.MOVIE_POSTER_URL}${Constants.MOVIE2.moviePosterUrl}",
                                    ),
                                ),
                            ),
                        error = null,
                    ),
                    awaitItem(),
                )

                cancelAndConsumeRemainingEvents()
            }
        }
}
