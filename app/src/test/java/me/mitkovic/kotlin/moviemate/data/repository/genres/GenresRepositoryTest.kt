package me.mitkovic.kotlin.moviemate.data.repository.genres

import app.cash.turbine.test
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.testing.data.repository.genres.TestGenresRepositoryImpl
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import kotlin.collections.any
import kotlin.collections.orEmpty

@OptIn(ExperimentalCoroutinesApi::class)
class GenresRepositoryTest {

    private val testDispatcher = StandardTestDispatcher()
    private val genresRepository: GenresRepository = TestGenresRepositoryImpl

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `getGenres returns expected genres`() =
        runTest {
            val genres =
                listOf(
                    Constants.GENRE_ACTION,
                    Constants.GENRE_COMEDY,
                )
            TestGenresRepositoryImpl.setGenres(genres)

            genresRepository.getGenres().test {
                val loadingState = awaitItem()
                Assert.assertTrue(loadingState is Resource.Loading)

                val result = awaitItem()
                Assert.assertTrue(result is Resource.Success)
                val genreResults = (result as Resource.Success).data?.genreResponses.orEmpty()
                Assert.assertEquals(2, genreResults.size)
                Assert.assertTrue(genreResults.any { it.name == Constants.GENRE_ACTION.name })
                Assert.assertTrue(genreResults.any { it.name == Constants.GENRE_COMEDY.name })

                cancelAndConsumeRemainingEvents()
            }
        }

    @Test
    fun `getGenres returns error when no genres available`() =
        runTest {
            TestGenresRepositoryImpl.setGenres(emptyList())

            genresRepository.getGenres().test {
                val loadingState = awaitItem()
                Assert.assertTrue(loadingState is Resource.Loading)

                val result = awaitItem()
                Assert.assertTrue(result is Resource.Error)
                Assert.assertEquals("No genres available", (result as Resource.Error).message)

                cancelAndConsumeRemainingEvents()
            }
        }
}
