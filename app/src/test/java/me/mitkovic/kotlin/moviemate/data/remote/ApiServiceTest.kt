package me.mitkovic.kotlin.moviemate.data.remote

import kotlinx.coroutines.test.runTest
import me.mitkovic.kotlin.moviemate.common.Constants
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiServiceTest {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var apiService: ApiService

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        val retrofit =
            Retrofit
                .Builder()
                .baseUrl(mockWebServer.url("/"))
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    private fun enqueueMockResponse(
        body: String,
        code: Int = 200,
    ) {
        val mockResponse =
            MockResponse()
                .setResponseCode(code)
                .setBody(body.trimIndent())
        mockWebServer.enqueue(mockResponse)
    }

    private fun <T> assertResponse(response: retrofit2.Response<T>) {
        assertTrue(response.isSuccessful)
        val responseBody = response.body()
        assertNotNull(responseBody)
    }

    @Test
    fun `fetch popular movies returns valid response`() =
        runTest {
            // Define the JSON response
            val mockResponseBody = """
            {
                "page": 1,
                "results": [
                    {
                        "id": 1,
                        "title": "Movie 1",
                        "vote_average": 8.5,
                        "poster_path": "url1",
                        "vote_count": 100,
                        "release_date": "2020",
                        "overview": "Overview 1",
                        "genres": []
                    },
                    {
                        "id": 2,
                        "title": "Movie 2",
                        "vote_average": 7.0,
                        "poster_path": "url2",
                        "vote_count": 150,
                        "release_date": "2021",
                        "overview": "Overview 2",
                        "genres": []
                    }
                ]
            }
        """
            enqueueMockResponse(mockResponseBody)

            // Make the API call
            val response = apiService.getPopularMovies(1, Constants.API_KEY)

            // Assert response
            assertResponse(response)

            val responseBody = response.body()
            assertEquals(2, responseBody?.results?.size)
            assertEquals("Movie 1", responseBody?.results?.get(0)?.title)
        }

    @Test
    fun `fetch movie details returns valid response`() =
        runTest {
            // Define the JSON response
            val mockResponseBody = """
            {
                "id": 1,
                "title": "Movie 1",
                "vote_average": 8.5,
                "poster_path": "url1",
                "vote_count": 100,
                "release_date": "2020",
                "overview": "Overview 1",
                "genres": []
            }
        """
            enqueueMockResponse(mockResponseBody)

            // Make the API call
            val response = apiService.getMovieDetails(1, Constants.API_KEY)

            // Assert response
            assertResponse(response)

            val responseBody = response.body()
            assertEquals("Movie 1", responseBody?.title)
        }

    @Test
    fun `fetch similar movies returns valid response`() =
        runTest {
            // Define the JSON response
            val mockResponseBody = """
            {
                "page": 1,
                "results": [
                    {
                        "id": 2,
                        "title": "Movie 2",
                        "vote_average": 7.0,
                        "poster_path": "url2",
                        "vote_count": 150,
                        "release_date": "2021",
                        "overview": "Overview 2",
                        "genres": []
                    }
                ]
            }
        """
            enqueueMockResponse(mockResponseBody)

            // Make the API call
            val response = apiService.getSimilarMovies(1, Constants.API_KEY)

            // Assert response
            assertResponse(response)

            val responseBody = response.body()
            assertEquals(1, responseBody?.results?.size)
            assertEquals("Movie 2", responseBody?.results?.get(0)?.title)
        }

    @Test
    fun `search movies returns valid response`() =
        runTest {
            // Define the JSON response
            val mockResponseBody = """
            {
                "page": 1,
                "results": [
                    {
                        "id": 1,
                        "title": "Movie 1",
                        "vote_average": 8.5,
                        "poster_path": "url1",
                        "vote_count": 100,
                        "release_date": "2020",
                        "overview": "Overview 1",
                        "genres": []
                    }
                ]
            }
        """
            enqueueMockResponse(mockResponseBody)

            // Make the API call
            val response = apiService.searchMovie("Movie 1", 1, Constants.API_KEY)

            // Assert response
            assertResponse(response)

            val responseBody = response.body()
            assertEquals(1, responseBody?.results?.size)
            assertEquals("Movie 1", responseBody?.results?.get(0)?.title)
        }
}
