package me.mitkovic.kotlin.moviemate.data.model

data class GenreResponse(
    val id: Int?,
    val name: String?
)
