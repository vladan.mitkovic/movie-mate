package me.mitkovic.kotlin.moviemate.data.remote

import me.mitkovic.kotlin.moviemate.data.model.GenresResponse
import me.mitkovic.kotlin.moviemate.data.model.MoviesResponse
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import retrofit2.Response

interface RemoteDataSource {
    suspend fun getPopularMovies(page: Int): Response<MoviesResponse>

    suspend fun getMovieDetails(movieId: Int): Response<Movie>

    suspend fun getSimilarMovies(movieId: Int): Response<MoviesResponse>

    suspend fun searchMovie(
        name: String,
        page: Int,
    ): Response<MoviesResponse>

    suspend fun getMovieGenres(): Response<GenresResponse>
}
