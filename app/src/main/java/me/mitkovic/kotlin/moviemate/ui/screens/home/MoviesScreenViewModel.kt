package me.mitkovic.kotlin.moviemate.ui.screens.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.filter
import androidx.paging.map
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.data.model.GenresResponse
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.data.repository.MovieMateRepository
import me.mitkovic.kotlin.moviemate.logging.AppLogger
import me.mitkovic.kotlin.moviemate.ui.utils.NumberUtils.roundOffDecimal

@OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
class MoviesScreenViewModel(
    private val movieMateRepository: MovieMateRepository,
    private val logger: AppLogger,
) : ViewModel() {

    private val _selectedGenreId = MutableStateFlow<Int?>(null)
    val selectedGenreId: StateFlow<Int?> = _selectedGenreId

    private val _searchText = MutableStateFlow("")
    val searchText: StateFlow<String> = _searchText

    private val _error = MutableStateFlow<String?>(null)
    val error = _error.asStateFlow()

    val genresUiState: StateFlow<Resource<GenresResponse>> =
        movieMateRepository
            .genresRepository
            .getGenres()
            .catch { e ->
                _error.value = e.message ?: Constants.SOMETHING_WENT_WRONG
                emit(Resource.Error(e.message ?: Constants.SOMETHING_WENT_WRONG))
            }.distinctUntilChanged()
            .stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5_000),
                initialValue = Resource.Loading(),
            )

    val pagedMoviesUiState: StateFlow<PagingData<Movie>> =
        combine(_searchText, _selectedGenreId) { query, genreId ->
            Pair(query, genreId)
        }.flatMapLatest { (query, genreId) ->
            when {
                query.isEmpty() -> getPopularMovies(genreId)
                else -> searchMoviesList(query, genreId)
            }
        }.cachedIn(viewModelScope)
            .stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5_000),
                initialValue = PagingData.empty(),
            )

    private fun getPopularMovies(genreId: Int?) =
        movieMateRepository
            .moviesRepository
            .getPopularMovies { throwable ->
                handleError("Error fetching Movies", throwable)
            }.map { pagingData ->
                pagingData
                    .filter { movie ->
                        genreId == null || movie.genreIds?.contains(genreId) == true
                    }.map { movie ->
                        movie.copy(
                            averageVote = movie.averageVote?.roundOffDecimal(),
                            moviePosterUrl = "${Constants.MOVIE_POSTER_URL}${movie.moviePosterUrl}",
                        )
                    }
            }

    private suspend fun searchMoviesList(
        query: String,
        genreId: Int?,
    ) = movieMateRepository
        .moviesRepository
        .searchMovies(name = query)
        .debounce(DEBOUNCE_TIMEOUT)
        .map { pagingData ->
            pagingData
                .filter { movie ->
                    genreId == null || movie.genreIds?.contains(genreId) == true
                }.map { movie ->
                    movie.copy(
                        averageVote = movie.averageVote?.roundOffDecimal(),
                        moviePosterUrl = "${Constants.MOVIE_POSTER_URL}${movie.moviePosterUrl}",
                    )
                }
        }.catch { e ->
            handleError("Error searching Movies", e)
            emit(PagingData.empty())
        }

    private fun handleError(
        message: String,
        throwable: Throwable,
    ) {
        _error.value = throwable.message ?: "Unknown Error Occurred"
        // logger.logError("$message: ${throwable.message}", throwable)
    }

    fun setSelectedGenre(genreId: Int?) {
        _selectedGenreId.value = genreId
    }

    fun logMessage(message: String) = logger.logError(message, null, "MovieMate")

    fun clearError() {
        _error.value = null
    }

    fun setSearchText(searchText: String) {
        _searchText.value = searchText
    }

    companion object {
        private const val DEBOUNCE_TIMEOUT = 300L
    }
}
