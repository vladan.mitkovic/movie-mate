package me.mitkovic.kotlin.moviemate.data.model

import me.mitkovic.kotlin.moviemate.data.model.database.Movie

data class MoviesResponse(
    val page: Int,
    val results: List<Movie>
)
