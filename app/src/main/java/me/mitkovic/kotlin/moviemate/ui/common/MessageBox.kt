package me.mitkovic.kotlin.moviemate.ui.common

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import me.mitkovic.kotlin.moviemate.ui.theme.fontSizes
import me.mitkovic.kotlin.moviemate.ui.theme.spacing
import me.mitkovic.kotlin.moviemate.ui.utils.DevicePreviews

@Composable
fun MessageBox(
    message: String,
    horizontalPadding: Dp,
    fontSize: TextUnit,
    color: Color,
) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center,
    ) {
        Text(
            text = message,
            modifier = Modifier.padding(horizontal = horizontalPadding),
            color = color,
            fontSize = fontSize,
        )
    }
}

@Composable
@DevicePreviews
fun MessageBoxDarkModePreview() {
    DarkModePreview {
        MessageBox(
            message = "No Movies Found",
            horizontalPadding = MaterialTheme.spacing.medium,
            fontSize = MaterialTheme.fontSizes.medium,
            color = MaterialTheme.colorScheme.onBackground,
        )
    }
}

@Composable
@DevicePreviews
fun MessageBoxLightModePreview() {
    LightModePreview {
        MessageBox(
            message = "No Movies Found",
            horizontalPadding = MaterialTheme.spacing.medium,
            fontSize = MaterialTheme.fontSizes.medium,
            color = MaterialTheme.colorScheme.onBackground,
        )
    }
}
