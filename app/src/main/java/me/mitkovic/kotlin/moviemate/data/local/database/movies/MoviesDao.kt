package me.mitkovic.kotlin.moviemate.data.local.database.movies

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.firstOrNull
import me.mitkovic.kotlin.moviemate.data.model.database.Movie

@Dao
interface MoviesDao {
    // Add ORDER BY id ASC to ensure consistent ordering
    @Query("SELECT * FROM movies ORDER BY `order` ASC")
    fun getPagedMovies(): PagingSource<Int, Movie>

    @Query("SELECT * FROM movies ORDER BY `order`")
    fun getAllMovies(): Flow<List<Movie>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovieDetail(movieDetail: Movie)

    @Update
    suspend fun updateMovie(movie: Movie)

    @Query("SELECT * FROM movies WHERE id = :movieId")
    fun getMovieDetail(movieId: Int): Flow<Movie?>

    @Query("SELECT * FROM movies WHERE title LIKE '%' || :name || '%'")
    fun searchMovies(name: String): Flow<List<Movie>>

    @Query("SELECT MAX(`order`) FROM movies")
    suspend fun getMaxMovieOrder(): Int?

    @Transaction
    suspend fun upsertMovies(movies: List<Movie>) {
        val currentMaxOrder = getMaxMovieOrder() ?: 0

        movies.forEachIndexed { index, newMovie ->
            val existingMovie = getMovieDetail(newMovie.id).firstOrNull()

            if (existingMovie != null) {
                // Merge existing movie with new data
                val mergedMovie =
                    existingMovie.copy(
                        // Preserve more complete data
                        title = newMovie.title ?: existingMovie.title,
                        averageVote = newMovie.averageVote ?: existingMovie.averageVote,
                        moviePosterUrl = newMovie.moviePosterUrl ?: existingMovie.moviePosterUrl,
                        totalVotes = newMovie.totalVotes ?: existingMovie.totalVotes,
                        releaseDate = newMovie.releaseDate ?: existingMovie.releaseDate,
                        overview = newMovie.overview ?: existingMovie.overview,
                        genreIds = newMovie.genreIds ?: existingMovie.genreIds,
                        // Preserve the existing order
                        order = existingMovie.order,
                        isFavorite = existingMovie.isFavorite,
                    )

                updateMovie(mergedMovie)
            } else {
                // If movie doesn't exist, insert it with a new incremented order
                val movieWithOrder =
                    newMovie.copy(
                        order = currentMaxOrder + index + 1,
                    )
                insertMovieDetail(movieWithOrder)
            }
        }
    }

    @Query("DELETE FROM movies")
    suspend fun deleteAllMovies()

    // Favorites

    @Query("SELECT * FROM movies WHERE isFavorite = 1")
    fun getFavorites(): Flow<List<Movie>>

    @Query("UPDATE movies SET isFavorite = :favorite WHERE id = :movieId")
    suspend fun updateFavoriteStatus(
        movieId: Int,
        favorite: Boolean,
    )
}
