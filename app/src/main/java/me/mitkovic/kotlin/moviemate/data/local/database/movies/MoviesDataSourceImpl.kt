package me.mitkovic.kotlin.moviemate.data.local.database.movies

import androidx.paging.PagingSource
import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.moviemate.data.model.database.Movie

class MoviesDataSourceImpl(
    private val moviesDao: MoviesDao,
) : MoviesDataSource {

    // load paged movies from local database
    override fun getMovies(): PagingSource<Int, Movie> = moviesDao.getPagedMovies()

    override suspend fun saveMovies(movies: List<Movie>) {
        moviesDao.upsertMovies(movies)
    }

    override fun getAllMovies(): Flow<List<Movie>> = moviesDao.getAllMovies()

    override suspend fun saveMovieDetails(movieDetails: Movie) {
        moviesDao.upsertMovies(listOf(movieDetails))
    }

    override fun getMovieDetails(movieId: Int): Flow<Movie?> = moviesDao.getMovieDetail(movieId)

    override fun searchMovies(name: String): Flow<List<Movie>> = moviesDao.searchMovies(name)

    override suspend fun deleteAllMovies() = moviesDao.deleteAllMovies()

    override fun getFavoriteMovies(): Flow<List<Movie>> = moviesDao.getFavorites()

    override suspend fun updateMovieFavoriteStatus(
        movieId: Int,
        isFavorite: Boolean,
    ) {
        moviesDao.updateFavoriteStatus(movieId, isFavorite)
    }
}
