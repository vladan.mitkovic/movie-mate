package me.mitkovic.kotlin.moviemate.data.model.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movies")
data class Movie(
    @PrimaryKey val id: Int,
    val title: String?,
    @SerializedName("vote_average")
    val averageVote: Double?,
    @SerializedName("poster_path")
    val moviePosterUrl: String?,
    @SerializedName("vote_count")
    val totalVotes: Int?,
    @SerializedName("release_date")
    val releaseDate: String?,
    val overview: String?,
    @SerializedName("genre_ids")
    val genreIds: List<Int>?,
    val order: Int,
    val isFavorite: Boolean = false,
)
