package me.mitkovic.kotlin.moviemate.testing.ui.screens.home

import me.mitkovic.kotlin.moviemate.testing.common.TestAppLoggerImpl
import me.mitkovic.kotlin.moviemate.testing.data.repository.TestMovieMateRepositoryImpl
import me.mitkovic.kotlin.moviemate.ui.screens.home.MoviesScreenViewModel

object TestMoviesScreenViewModelProvider {

    fun provideMoviesScreenViewModel(): MoviesScreenViewModel =
        MoviesScreenViewModel(
            movieMateRepository = TestMovieMateRepositoryImpl,
            logger = TestAppLoggerImpl,
        )
}
