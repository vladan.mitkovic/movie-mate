package me.mitkovic.kotlin.moviemate.ui.utils

import android.content.res.Configuration.UI_MODE_NIGHT_NO
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.ui.tooling.preview.Preview

@Preview(
    name = "phone",
    device = "spec:width=360dp,height=640dp,dpi=480",
    showBackground = true,
    uiMode = UI_MODE_NIGHT_NO,
    group = "light",
)
@Preview(
    name = "landscape",
    device = "spec:width=640dp,height=360dp,dpi=480",
    showBackground = true,
    uiMode = UI_MODE_NIGHT_NO,
    group = "light",
)
@Preview(
    name = "foldable",
    device = "spec:width=673dp,height=841dp,dpi=480",
    showBackground = true,
    uiMode = UI_MODE_NIGHT_NO,
    group = "light",
)
@Preview(
    name = "tablet",
    device = "spec:width=1280dp,height=800dp,dpi=480",
    showBackground = true,
    uiMode = UI_MODE_NIGHT_NO,
    group = "light",
)
@Preview(
    name = "phone",
    device = "spec:width=360dp,height=640dp,dpi=480",
    showBackground = true,
    uiMode = UI_MODE_NIGHT_YES,
    group = "dark",
)
@Preview(
    name = "landscape",
    device = "spec:width=640dp,height=360dp,dpi=480",
    showBackground = true,
    uiMode = UI_MODE_NIGHT_YES,
    group = "dark",
)
@Preview(
    name = "foldable",
    device = "spec:width=673dp,height=841dp,dpi=480",
    showBackground = true,
    uiMode = UI_MODE_NIGHT_YES,
    group = "dark",
)
@Preview(
    name = "tablet",
    device = "spec:width=1280dp,height=800dp,dpi=480",
    showBackground = true,
    uiMode = UI_MODE_NIGHT_YES,
    group = "dark",
)
annotation class DevicePreviews
