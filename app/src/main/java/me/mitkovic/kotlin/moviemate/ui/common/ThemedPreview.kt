package me.mitkovic.kotlin.moviemate.ui.common

import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import me.mitkovic.kotlin.moviemate.ui.theme.MovieMateTheme

@Composable
fun DarkModePreview(content: @Composable () -> Unit) {
    MovieMateTheme(theme = AppCompatDelegate.MODE_NIGHT_YES) {
        Surface(color = MaterialTheme.colorScheme.background) {
            content()
        }
    }
}

@Composable
fun LightModePreview(content: @Composable () -> Unit) {
    MovieMateTheme(theme = AppCompatDelegate.MODE_NIGHT_NO) {
        Surface(color = MaterialTheme.colorScheme.background) {
            content()
        }
    }
}
