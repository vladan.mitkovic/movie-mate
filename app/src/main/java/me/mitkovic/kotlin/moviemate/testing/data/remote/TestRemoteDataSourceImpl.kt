package me.mitkovic.kotlin.moviemate.testing.data.remote

import me.mitkovic.kotlin.moviemate.data.model.GenresResponse
import me.mitkovic.kotlin.moviemate.data.model.MoviesResponse
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.data.remote.RemoteDataSource
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.Response

object TestRemoteDataSourceImpl : RemoteDataSource {

    private var popularMovies: List<Movie> = emptyList()
    private var movieDetails: Map<Int, Movie> = emptyMap()
    private var similarMovies: Map<Int, List<Movie>> = emptyMap()
    private var searchResults: List<Movie> = emptyList()
    private var genres: GenresResponse? = null

    fun setPopularMovies(movies: List<Movie>) {
        popularMovies = movies
    }

    fun setMovieDetails(details: Map<Int, Movie>) {
        movieDetails = details
    }

    fun setSimilarMovies(similar: Map<Int, List<Movie>>) {
        similarMovies = similar
    }

    fun setSearchResults(results: List<Movie>) {
        searchResults = results
    }

    fun setGenresResponse(genresResponse: GenresResponse?) {
        genres = genresResponse
    }

    override suspend fun getPopularMovies(page: Int): Response<MoviesResponse> = Response.success(MoviesResponse(page, popularMovies))

    override suspend fun getMovieDetails(movieId: Int): Response<Movie> {
        val details = movieDetails[movieId]
        return if (details != null) {
            Response.success(details)
        } else {
            Response.error(404, "{\"message\":\"Movie not found\"}".toResponseBody("application/json".toMediaTypeOrNull()))
        }
    }

    override suspend fun getSimilarMovies(movieId: Int): Response<MoviesResponse> {
        val similar = similarMovies[movieId] ?: emptyList()
        return Response.success(MoviesResponse(1, similar))
    }

    override suspend fun searchMovie(
        name: String,
        page: Int,
    ): Response<MoviesResponse> {
        val results = searchResults.filter { it.title?.contains(name, ignoreCase = true) == true }
        return Response.success(MoviesResponse(page, results))
    }

    override suspend fun getMovieGenres(): Response<GenresResponse> =
        genres?.let { Response.success(it) }
            ?: Response.error(404, "{\"message\":\"Genres not found\"}".toResponseBody("application/json".toMediaTypeOrNull()))
}
