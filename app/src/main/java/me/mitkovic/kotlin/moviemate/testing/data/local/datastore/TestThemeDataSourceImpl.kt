package me.mitkovic.kotlin.moviemate.testing.data.local.datastore

import androidx.appcompat.app.AppCompatDelegate
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import me.mitkovic.kotlin.moviemate.data.local.datastore.ThemeDataSource

object TestThemeDataSourceImpl : ThemeDataSource {

    private val themeFlow = MutableStateFlow<Int>(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)

    override suspend fun saveTheme(themeValue: Int) {
        themeFlow.value = themeValue
    }

    override fun getTheme(): Flow<Int> = themeFlow
}
