package me.mitkovic.kotlin.moviemate.ui.screens.details

import androidx.compose.runtime.Immutable
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.common.Constants.SOMETHING_WENT_WRONG
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.data.repository.MovieMateRepository
import me.mitkovic.kotlin.moviemate.logging.AppLogger
import me.mitkovic.kotlin.moviemate.ui.utils.stateInWhileSubscribed
import me.mitkovic.kotlin.moviemate.ui.utils.toMovieDetailsUi

data class MovieDetailsUiState(
    val isLoading: Boolean = false,
    val movieDetails: MovieDetailsUi? = null,
    val similarMovies: SimilarMovies = SimilarMovies(emptyList()),
    val error: String? = null,
)

@Immutable
data class SimilarMovies(
    val movies: List<Movie>,
)

data class MovieDetailsUi(
    val id: Int?,
    val title: String?,
    val averageVote: Double?,
    val moviePosterUrl: String,
    val totalVotes: Int?,
    val releaseDate: String?,
    val overview: String?,
    val genreIds: String?,
    val isFavorite: Boolean = false,
)

class MovieDetailsScreenViewModel(
    private val movieMateRepository: MovieMateRepository,
    private val logger: AppLogger,
    savedStateHandle: SavedStateHandle,
) : ViewModel() {

    private val movieId: Int = checkNotNull(savedStateHandle["id"]) { "Movie ID is missing" }
    private val favoriteToggleFlow = MutableStateFlow(false)

    // Flow to fetch movie details and handle favorite status
    private val movieDetailsFlow =
        movieMateRepository
            .moviesRepository
            .getMovieDetails(movieId)
            .map { resource ->
                when (resource) {
                    is Resource.Loading -> MovieDetailsUiState(isLoading = true, error = null)
                    is Resource.Success -> {
                        val genreNames =
                            resource.data
                                ?.genreIds
                                ?.mapNotNull { genreId ->
                                    movieMateRepository
                                        .genresRepository
                                        .getGenreById(genreId)
                                        ?.name
                                }?.joinToString(", ") ?: Constants.UNKNOWN

                        val moviePosterUrl = "${Constants.MOVIE_POSTER_URL}${resource.data?.moviePosterUrl}"

                        val movieDetails =
                            resource.data?.toMovieDetailsUi(
                                movieMateRepository
                                    .moviesRepository,
                                moviePosterUrl,
                                genreNames,
                            )

                        // Update favoriteToggleFlow with the initial favorite status
                        favoriteToggleFlow.value = movieDetails?.isFavorite == true

                        MovieDetailsUiState(
                            isLoading = false,
                            movieDetails = movieDetails,
                            error = null,
                        )
                    }
                    is Resource.Error -> {
                        val errorMessage = resource.message ?: SOMETHING_WENT_WRONG
                        logger.logError("Error fetching Movie details", resource.exception)
                        MovieDetailsUiState(
                            isLoading = false,
                            error = errorMessage,
                        )
                    }
                }
            }.catch { e ->
                logger.logError("Error fetching Movie details", e)
                emit(MovieDetailsUiState(isLoading = false, error = SOMETHING_WENT_WRONG))
            }.stateInWhileSubscribed(
                viewModel = this,
                initialValue = MovieDetailsUiState(),
            )

    // Flow to fetch similar movies
    private val similarMoviesFlow =
        movieMateRepository
            .moviesRepository
            .getSimilarMovies(movieId)
            .map { resource ->
                when (resource) {
                    is Resource.Loading -> SimilarMovies(emptyList())
                    is Resource.Success -> {
                        val updatedMovies =
                            resource.data?.results?.map { similarMovie ->
                                similarMovie.copy(
                                    moviePosterUrl = "${Constants.MOVIE_POSTER_URL}${similarMovie.moviePosterUrl}",
                                )
                            } ?: emptyList()
                        SimilarMovies(updatedMovies)
                    }
                    is Resource.Error -> {
                        logger.logError(resource.message ?: SOMETHING_WENT_WRONG, resource.exception)
                        SimilarMovies(emptyList()) // Represents error state by emitting empty list
                    }
                }
            }.onStart { emit(SimilarMovies(emptyList())) }
            .catch { e ->
                logger.logError("Error fetching similar movies", e)
                emit(SimilarMovies(emptyList()))
            }.stateInWhileSubscribed(
                viewModel = this,
                initialValue = SimilarMovies(emptyList()),
            )

    // Combine movie details with favorite status
    private val combinedDetailsFlow =
        movieDetailsFlow
            .combine(favoriteToggleFlow) { uiState, isFavorite ->
                uiState.copy(movieDetails = uiState.movieDetails?.copy(isFavorite = isFavorite))
            }

    // Final UI state combining movie details and similar movies
    val movieDetailsUiState: StateFlow<MovieDetailsUiState> =
        combinedDetailsFlow
            .combine(similarMoviesFlow) { detailsState, similarMovies ->
                detailsState.copy(
                    similarMovies = similarMovies,
                    isLoading = detailsState.isLoading,
                    error = detailsState.error,
                )
            }.onStart { emit(MovieDetailsUiState(isLoading = true, error = null)) }
            .catch { e ->
                logger.logError("Error in movie details state flow", e)
                emit(MovieDetailsUiState(isLoading = false, error = SOMETHING_WENT_WRONG))
            }.stateInWhileSubscribed(
                viewModel = this,
                initialValue = MovieDetailsUiState(),
            )

    fun toggleFavoriteStatus(id: Int) {
        viewModelScope.launch {
            try {
                val isCurrentlyFavorite =
                    movieMateRepository
                        .moviesRepository
                        .isFavorite(id)

                movieMateRepository
                    .moviesRepository
                    .updateMovieFavoriteStatus(id, !isCurrentlyFavorite)
                // Update favoriteToggleFlow to trigger UI updates
                favoriteToggleFlow.value = !isCurrentlyFavorite
            } catch (e: Exception) {
                logger.logError("Failed to update favorite status for Movie with ID: $id", e)
            }
        }
    }
}
