package me.mitkovic.kotlin.moviemate.data.repository.theme

import kotlinx.coroutines.flow.Flow

interface ThemeRepository {

    suspend fun saveTheme(themeValue: Int)

    fun getTheme(): Flow<Int>
}
