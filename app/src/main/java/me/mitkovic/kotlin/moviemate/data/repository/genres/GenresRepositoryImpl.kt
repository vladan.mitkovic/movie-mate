package me.mitkovic.kotlin.moviemate.data.repository.genres

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import me.mitkovic.kotlin.moviemate.data.local.LocalDataSource
import me.mitkovic.kotlin.moviemate.data.model.GenreResponse
import me.mitkovic.kotlin.moviemate.data.model.GenresResponse
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.data.remote.RemoteDataSource
import me.mitkovic.kotlin.moviemate.logging.AppLogger

class GenresRepositoryImpl(
    private val localDataSource: LocalDataSource,
    private val remoteDataSource: RemoteDataSource,
    private val logger: AppLogger,
) : GenresRepository {

    override fun getGenres(): Flow<Resource<GenresResponse>> =
        flow {
            emit(Resource.Loading())

            // Always emit cached data first
            val cachedGenres =
                localDataSource
                    .genresDataSource
                    .getGenres()
                    .first()
            if (cachedGenres.isNotEmpty()) {
                emit(Resource.Success(GenresResponse(genreResponses = cachedGenres)))
            }

            try {
                // Try to fetch fresh data from API
                val response = remoteDataSource.getMovieGenres()
                if (response.isSuccessful) {
                    response.body()?.let { apiGenres ->
                        localDataSource
                            .genresDataSource
                            .saveGenres(apiGenres.genreResponses)
                        // Only emit new API data if it's different from cache
                        if (apiGenres.genreResponses != cachedGenres) {
                            emit(Resource.Success(apiGenres))
                        }
                    }
                }
                // We don't emit errors since we want to keep showing cached data
            } catch (e: Exception) {
                // Log the error but don't emit it
                logger.logError("Error fetching genres", e)
                // No error emission - cached data remains displayed
            }
        }

    override suspend fun getGenreById(genreId: Int): GenreResponse? =
        localDataSource
            .genresDataSource
            .getGenreById(genreId)
}
