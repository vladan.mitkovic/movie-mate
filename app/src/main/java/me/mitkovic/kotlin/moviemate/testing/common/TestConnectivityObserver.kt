package me.mitkovic.kotlin.moviemate.testing.common

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import me.mitkovic.kotlin.moviemate.common.ConnectivityObserver

object TestConnectivityObserver : ConnectivityObserver {

    override fun observe(): Flow<ConnectivityObserver.Status> = emptyFlow()
}
