package me.mitkovic.kotlin.moviemate.data.local.database.movies

import androidx.paging.PagingSource
import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.moviemate.data.model.database.Movie

interface MoviesDataSource {

    suspend fun saveMovies(movies: List<Movie>)

    fun getAllMovies(): Flow<List<Movie>>

    fun getMovies(): PagingSource<Int, Movie>

    suspend fun saveMovieDetails(movieDetails: Movie)

    fun getMovieDetails(movieId: Int): Flow<Movie?>

    fun searchMovies(name: String): Flow<List<Movie>>

    suspend fun deleteAllMovies()

    fun getFavoriteMovies(): Flow<List<Movie>>

    suspend fun updateMovieFavoriteStatus(
        movieId: Int,
        isFavorite: Boolean,
    )
}
