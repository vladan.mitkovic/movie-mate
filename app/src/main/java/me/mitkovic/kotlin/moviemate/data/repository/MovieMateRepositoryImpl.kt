package me.mitkovic.kotlin.moviemate.data.repository

import me.mitkovic.kotlin.moviemate.data.repository.genres.GenresRepository
import me.mitkovic.kotlin.moviemate.data.repository.movies.MoviesRepository
import me.mitkovic.kotlin.moviemate.data.repository.theme.ThemeRepository

class MovieMateRepositoryImpl(
    override val moviesRepository: MoviesRepository,
    override val genresRepository: GenresRepository,
    override val themeRepository: ThemeRepository,
) : MovieMateRepository
