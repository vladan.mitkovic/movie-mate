package me.mitkovic.kotlin.moviemate.ui.utils

import androidx.navigation.NavController
import me.mitkovic.kotlin.moviemate.ui.navigation.BottomNavigation

object NavigationUtils {

    private val homeRoute = BottomNavigation.HOME.route::class.qualifiedName
    private val favoritesRoute = BottomNavigation.FAVORITES.route::class.qualifiedName
    private val settingsRoute = BottomNavigation.SETTINGS.route::class.qualifiedName
    private const val MOVIE_DETAILS_ROUTE_PATTERN = "MovieDetails/{id}"

    private var lastMainRoute: String? = null

    fun setLastMainRoute(route: String?) {
        lastMainRoute = route
    }

    /**
     * Determines if the current route corresponds to the target destination.
     *
     * @param currentRoute The current route from NavController.
     * @param targetRoute The target route string.
     * @param navController The NavController instance.
     * @return True if the current route matches the target destination or is a detail route for it.

     * Handling bottom navigation tab selection
     * When user goes to detail screen from Home, Detail screen Home tab has to stay selected.
     * The same for Favorites tabs. When user goes to Detail screen from Favorites tab, Detail screen or Favorite tab has to stay selected.
     */
    fun isSelectedRoute(
        currentRoute: String?,
        targetRoute: String,
        navController: NavController,
    ): Boolean {
        // Helper function to check if the current route is a detail route for the target route
        fun isDetailRouteFor(targetRoute: String): Boolean =
            currentRoute?.endsWith(MOVIE_DETAILS_ROUTE_PATTERN) == true &&
                (navController.previousBackStackEntry?.destination?.route == targetRoute || lastMainRoute == targetRoute)

        // Compare the current route with target route or check if it's a detail route
        return when (targetRoute) {
            homeRoute -> currentRoute == homeRoute || isDetailRouteFor(homeRoute)
            favoritesRoute -> currentRoute == favoritesRoute || isDetailRouteFor(favoritesRoute)
            settingsRoute -> currentRoute == settingsRoute
            else -> false
        }
    }
}
