package me.mitkovic.kotlin.moviemate.data.local.database.genres

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import me.mitkovic.kotlin.moviemate.data.model.GenreResponse
import me.mitkovic.kotlin.moviemate.data.model.database.Genre

class GenresDataSourceImpl(
    private val genresDao: GenresDao,
) : GenresDataSource {

    override suspend fun saveGenres(genres: List<GenreResponse>) {
        val genreEntities = genres.map { Genre(id = it.id ?: 0, name = it.name) }
        genresDao.insertGenres(genreEntities)
    }

    override fun getGenres(): Flow<List<GenreResponse>> =
        genresDao
            .getAllGenres()
            .map { genres ->
                genres.map { genre ->
                    GenreResponse(id = genre.id, name = genre.name)
                }
            }

    override suspend fun getGenreById(genreId: Int): GenreResponse? {
        val genre = genresDao.getGenreById(genreId)
        return genre?.let { GenreResponse(id = it.id, name = it.name) }
    }
}
