package me.mitkovic.kotlin.moviemate.data.repository.genres

import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.moviemate.data.model.GenreResponse
import me.mitkovic.kotlin.moviemate.data.model.GenresResponse
import me.mitkovic.kotlin.moviemate.data.model.Resource

interface GenresRepository {

    fun getGenres(): Flow<Resource<GenresResponse>>

    suspend fun getGenreById(genreId: Int): GenreResponse?
}
