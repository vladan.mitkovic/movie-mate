package me.mitkovic.kotlin.moviemate.testing.ui.screens.details

import androidx.lifecycle.SavedStateHandle
import me.mitkovic.kotlin.moviemate.testing.common.TestAppLoggerImpl
import me.mitkovic.kotlin.moviemate.testing.data.repository.TestMovieMateRepositoryImpl
import me.mitkovic.kotlin.moviemate.ui.screens.details.MovieDetailsScreenViewModel

object TestMovieDetailsViewModelProvider {

    fun provideMovieDetailsViewModel(movieId: Int): MovieDetailsScreenViewModel {
        val savedStateHandle = SavedStateHandle(mapOf("id" to movieId))
        return MovieDetailsScreenViewModel(
            movieMateRepository = TestMovieMateRepositoryImpl,
            logger = TestAppLoggerImpl,
            savedStateHandle = savedStateHandle,
        )
    }
}
