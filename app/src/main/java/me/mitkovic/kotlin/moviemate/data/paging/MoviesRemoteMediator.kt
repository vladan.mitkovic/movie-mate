package me.mitkovic.kotlin.moviemate.data.paging

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.paging.RemoteMediator.MediatorResult
import androidx.room.withTransaction
import me.mitkovic.kotlin.moviemate.data.local.database.MoviesDatabase
import me.mitkovic.kotlin.moviemate.data.local.database.movies.MoviesDataSource
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.data.remote.RemoteDataSource
import me.mitkovic.kotlin.moviemate.logging.AppLogger
import retrofit2.HttpException
import java.io.IOException

@OptIn(ExperimentalPagingApi::class)
class MoviesRemoteMediator(
    private val database: MoviesDatabase,
    private val remoteDataSource: RemoteDataSource,
    private val moviesDataSource: MoviesDataSource,
    private val logger: AppLogger,
    private val onRemoteError: (Throwable) -> Unit,
) : RemoteMediator<Int, Movie>() {

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, Movie>,
    ): MediatorResult {
        val currentPage =
            when (loadType) {
                LoadType.REFRESH -> 1
                LoadType.PREPEND -> return MediatorResult.Success(
                    endOfPaginationReached = true,
                )
                LoadType.APPEND -> {
                    val lastItem = state.lastItemOrNull()
                    if (lastItem == null) {
                        1
                    } else {
                        (lastItem.order / state.config.pageSize) + 1
                    }
                }
            }

        // logger.logError("currentPage: $currentPage", null, "MM")

        return try {
            // Fetch data from the remote API
            val response = remoteDataSource.getPopularMovies(currentPage)
            if (response.isSuccessful) {
                val body = response.body()
                val movies = body?.results ?: emptyList()

                // Calculate end-of-pagination state
                val endOfPaginationReached = movies.isEmpty()

                database.withTransaction {
                    // if (loadType == LoadType.REFRESH) {
                    // Deleting movies from database will delete all movies flagged as favorite.
                    // localDataSource.deleteAllMovies()
                    // }

                    moviesDataSource.saveMovies(movies = movies)
                }

                // logger.logError("movies: ${movies.map { it.id }}", null, "MM")

                // Return the MediatorResult
                MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
            } else {
                logger.logError("API response failed with code: ${response.code()}", null)
                onRemoteError(HttpException(response))
                MediatorResult.Error(HttpException(response))
            }
        } catch (e: IOException) {
            // If there's a network error, we should gracefully stop pagination and use local data
            logger.logError("No internet connection or network issue: $e", e)
            onRemoteError(e)
            MediatorResult.Success(endOfPaginationReached = true)
        } catch (e: Exception) {
            logger.logError("Error fetching data: ${e.message}", e)
            onRemoteError(e)
            MediatorResult.Error(e)
        }
    }
}
