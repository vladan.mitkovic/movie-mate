package me.mitkovic.kotlin.moviemate.ui.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import me.mitkovic.kotlin.moviemate.data.model.GenreResponse
import me.mitkovic.kotlin.moviemate.data.model.GenresResponse
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.data.repository.movies.MoviesRepository
import me.mitkovic.kotlin.moviemate.ui.screens.details.MovieDetailsUi

fun <T> Flow<T>.stateInWhileSubscribed(
    viewModel: ViewModel,
    initialValue: T,
): StateFlow<T> =
    stateIn(
        scope = viewModel.viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000),
        initialValue = initialValue,
    )

suspend fun Movie.toMovieDetailsUi(
    moviesRepository: MoviesRepository,
    moviePosterUrl: String,
    genreNames: String,
): MovieDetailsUi =
    MovieDetailsUi(
        id = id,
        title = title,
        averageVote = averageVote,
        moviePosterUrl = moviePosterUrl,
        totalVotes = totalVotes,
        releaseDate = releaseDate,
        overview = overview,
        genreIds = genreNames,
        isFavorite = moviesRepository.isFavorite(id),
    )

fun Resource<GenresResponse>.getGenresOrEmpty(): List<GenreResponse> =
    if (this is Resource.Success) {
        this.data?.genreResponses ?: emptyList()
    } else {
        emptyList()
    }
