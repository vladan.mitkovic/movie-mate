package me.mitkovic.kotlin.moviemate.data.local.database.genres

import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.moviemate.data.model.GenreResponse

interface GenresDataSource {

    suspend fun saveGenres(genres: List<GenreResponse>)

    fun getGenres(): Flow<List<GenreResponse>>

    suspend fun getGenreById(genreId: Int): GenreResponse?
}
