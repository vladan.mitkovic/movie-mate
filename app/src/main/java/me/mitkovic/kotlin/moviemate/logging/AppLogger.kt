package me.mitkovic.kotlin.moviemate.logging

interface AppLogger {

    fun logDebug(message: String)

    fun logDebugWithTag(
        tag: String,
        message: String,
    )

    fun logError(
        message: String?,
        throwable: Throwable?,
        tag: String? = null,
    )
}
