package me.mitkovic.kotlin.moviemate.testing.data.repository.theme

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import me.mitkovic.kotlin.moviemate.data.repository.theme.ThemeRepository

object TestThemeRepositoryImpl : ThemeRepository {

    override suspend fun saveTheme(themeValue: Int) {
        // No-op
    }

    override fun getTheme(): Flow<Int> {
        // No-op
        return flowOf(1)
    }
}
