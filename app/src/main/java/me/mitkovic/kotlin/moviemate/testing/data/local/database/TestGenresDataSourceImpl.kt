package me.mitkovic.kotlin.moviemate.testing.data.local.database

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import me.mitkovic.kotlin.moviemate.data.local.database.genres.GenresDataSource
import me.mitkovic.kotlin.moviemate.data.model.GenreResponse

object TestGenresDataSourceImpl : GenresDataSource {

    private val genresFlow = MutableStateFlow<List<GenreResponse>>(emptyList())

    override suspend fun saveGenres(genres: List<GenreResponse>) {
        genresFlow.value = genres
    }

    override fun getGenres(): Flow<List<GenreResponse>> = genresFlow

    override suspend fun getGenreById(genreId: Int): GenreResponse? = genresFlow.value.find { it.id == genreId }
}
