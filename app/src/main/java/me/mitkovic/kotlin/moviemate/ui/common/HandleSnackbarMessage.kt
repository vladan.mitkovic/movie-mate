package me.mitkovic.kotlin.moviemate.ui.common

import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect

@Composable
fun HandleSnackbarMessage(
    snackbarHostState: SnackbarHostState,
    snackbarMessage: Pair<String, Long>?,
) {
    // Use the timestamp as the key for LaunchedEffect
    LaunchedEffect(snackbarMessage?.second) {
        snackbarMessage?.first?.let { message ->
            // Dismiss any existing snackbar
            snackbarHostState.currentSnackbarData?.dismiss()
            // Show the snackbar
            snackbarHostState.showSnackbar(
                message = message,
                duration = SnackbarDuration.Short,
            )
        }
    }
}
