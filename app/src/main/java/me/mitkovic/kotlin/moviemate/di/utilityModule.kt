package me.mitkovic.kotlin.moviemate.di

import me.mitkovic.kotlin.moviemate.common.ConnectivityObserver
import me.mitkovic.kotlin.moviemate.common.NetworkConnectivityObserver
import me.mitkovic.kotlin.moviemate.logging.AppLogger
import me.mitkovic.kotlin.moviemate.logging.TimberLogger
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val utilityModule =
    module {

        // **Logger**
        single<AppLogger> { TimberLogger() }

        // **Connectivity Observer**
        single<ConnectivityObserver> { NetworkConnectivityObserver(androidContext()) }
    }
