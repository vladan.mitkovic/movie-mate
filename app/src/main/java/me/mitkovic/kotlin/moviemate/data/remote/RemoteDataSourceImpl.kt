package me.mitkovic.kotlin.moviemate.data.remote

import me.mitkovic.kotlin.moviemate.data.model.GenresResponse
import me.mitkovic.kotlin.moviemate.data.model.MoviesResponse
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import retrofit2.Response

class RemoteDataSourceImpl(
    private val apiService: ApiService,
) : RemoteDataSource {

    override suspend fun getPopularMovies(page: Int): Response<MoviesResponse> = apiService.getPopularMovies(page = page)

    override suspend fun getMovieDetails(movieId: Int): Response<Movie> = apiService.getMovieDetails(movieId)

    override suspend fun getSimilarMovies(movieId: Int): Response<MoviesResponse> = apiService.getSimilarMovies(movieId)

    override suspend fun searchMovie(
        name: String,
        page: Int,
    ): Response<MoviesResponse> = apiService.searchMovie(movieName = name, page = page)

    override suspend fun getMovieGenres(): Response<GenresResponse> = apiService.getMovieGenres()
}
