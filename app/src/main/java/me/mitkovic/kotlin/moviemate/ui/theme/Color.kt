package me.mitkovic.kotlin.moviemate.ui.theme

import androidx.compose.ui.graphics.Color

// Dark Theme Colors
val DarkPrimary = Color(0xFFBB86FC)
val DarkOnPrimary = Color(0xFF000000)
val DarkBackground = Color(0xFF1E3131)
val DarkOnBackground = Color(0xFFFFFFFF)
val DarkSurface = Color(0xFF1F1F1F)
val DarkOnSurface = Color(0xFFD0D0D0)

// Light Theme Colors
val LightPrimary = Color(0xFF6200EE)
val LightOnPrimary = Color(0xFFFFFFFF)
val LightBackground = Color(0xFFFFFFFF)
val LightOnBackground = Color(0xFF000000)
val LightSurface = Color(0xFFEFEFEF)
val LightOnSurface = Color(0xFF000000)

var TitleColor = Color(0xFFFE0A58)
val StarColor = Color(0xFFff9e22)
