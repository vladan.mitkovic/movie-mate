package me.mitkovic.kotlin.moviemate.ui.screens.home

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyGridState
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import coil.compose.AsyncImage
import me.mitkovic.kotlin.moviemate.R
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.testing.data.repository.movies.TestMoviesRepositoryImpl
import me.mitkovic.kotlin.moviemate.testing.ui.screens.home.TestMoviesScreenViewModelProvider
import me.mitkovic.kotlin.moviemate.ui.common.DarkModePreview
import me.mitkovic.kotlin.moviemate.ui.common.LoadingIndicator
import me.mitkovic.kotlin.moviemate.ui.common.MessageBox
import me.mitkovic.kotlin.moviemate.ui.theme.Spacing
import me.mitkovic.kotlin.moviemate.ui.theme.StarColor
import me.mitkovic.kotlin.moviemate.ui.theme.TitleColor
import me.mitkovic.kotlin.moviemate.ui.theme.fontSizes
import me.mitkovic.kotlin.moviemate.ui.theme.spacing
import me.mitkovic.kotlin.moviemate.ui.utils.DevicePreviews
import me.mitkovic.kotlin.moviemate.ui.utils.getGenresOrEmpty
import org.koin.androidx.compose.koinViewModel
import java.util.Locale

sealed class SearchAction {
    data class SearchTextChanged(
        val newText: String,
    ) : SearchAction()

    data class ShowSearchFieldChanged(
        val newValue: Boolean,
    ) : SearchAction()

    data object CloseSearchField : SearchAction()
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun MoviesScreen(
    viewModel: MoviesScreenViewModel = koinViewModel(),
    paddingValues: PaddingValues,
    onError: (String) -> Unit,
    onMovieClicked: (Int) -> Unit,
) {
    val spacing = MaterialTheme.spacing
    val fontSizes = MaterialTheme.fontSizes

    val pagedItems = viewModel.pagedMoviesUiState.collectAsLazyPagingItems()

    val genresUiState by viewModel.genresUiState.collectAsStateWithLifecycle()
    val selectedGenreId by viewModel.selectedGenreId.collectAsStateWithLifecycle()
    val error by viewModel.error.collectAsStateWithLifecycle()
    val searchText = viewModel.searchText.collectAsStateWithLifecycle()
    var showSearchField by rememberSaveable { mutableStateOf(false) }

    // If error happens, post it up the tree until the MainActivity
    // And handle it into Scaffold SnackbarHost
    LaunchedEffect(error) {
        error?.let { message ->
            onError(message)
            viewModel.clearError()
        }
    }

    Box(
        modifier =
            Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.background),
    ) {
        Column(modifier = Modifier.padding(paddingValues)) {
            val setSearchText = viewModel::setSearchText

            Row(
                modifier =
                    Modifier
                        .fillMaxWidth()
                        .padding(horizontal = spacing.small, vertical = spacing.small),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                if (!showSearchField) {
                    Text(
                        modifier = Modifier.padding(start = spacing.small),
                        text = stringResource(id = R.string.app_name),
                        style = MaterialTheme.typography.headlineLarge.copy(fontWeight = FontWeight.ExtraBold),
                        color = TitleColor,
                    )
                }
                SearchFieldWithCloseButton(
                    searchText = searchText.value,
                    showSearchField = showSearchField,
                    requestFocus = showSearchField && searchText.value.isEmpty(),
                    onSearchAction = { searchAction ->
                        when (searchAction) {
                            is SearchAction.SearchTextChanged -> {
                                setSearchText(searchAction.newText)
                            }
                            is SearchAction.ShowSearchFieldChanged -> {
                                showSearchField = searchAction.newValue
                            }
                            is SearchAction.CloseSearchField -> {
                                showSearchField = false
                                setSearchText("")
                            }
                        }
                    },
                )
            }

            // Handle Loading State
            if (genresUiState is Resource.Loading) {
                Box(
                    modifier = Modifier.fillMaxWidth(),
                    contentAlignment = Alignment.Center,
                ) {
                    CircularProgressIndicator(color = MaterialTheme.colorScheme.primary)
                }
            }

            // Handle Success State
            if (genresUiState is Resource.Success) {
                val genres = genresUiState.getGenresOrEmpty()
                LazyRow(
                    contentPadding =
                        PaddingValues(
                            start = spacing.small,
                            end = spacing.small,
                            top = spacing.medium,
                            bottom = spacing.medium,
                        ),
                ) {
                    item {
                        val isSelected = selectedGenreId == null
                        Text(
                            text = stringResource(R.string.all),
                            modifier =
                                Modifier
                                    .padding(horizontal = spacing.small)
                                    .clip(RoundedCornerShape(spacing.small))
                                    .background(
                                        if (isSelected) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.surface,
                                    ).clickable {
                                        viewModel.setSelectedGenre(null)
                                    }.padding(horizontal = spacing.medium, vertical = spacing.extraSmall),
                            style = MaterialTheme.typography.bodyLarge,
                            color = if (isSelected) MaterialTheme.colorScheme.onPrimary else MaterialTheme.colorScheme.onBackground,
                        )
                    }
                    items(genres.size) { index ->
                        val genre = genres[index]
                        val isSelected = selectedGenreId == genre.id
                        Text(
                            text = genre.name ?: "---",
                            modifier =
                                Modifier
                                    .padding(horizontal = spacing.small)
                                    .clip(RoundedCornerShape(spacing.small))
                                    .background(
                                        if (isSelected) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.surface,
                                    ).clickable {
                                        genre.id?.let { viewModel.setSelectedGenre(it) }
                                    }.padding(horizontal = spacing.medium, vertical = spacing.extraSmall),
                            style = MaterialTheme.typography.bodyLarge,
                            color = if (isSelected) MaterialTheme.colorScheme.onPrimary else MaterialTheme.colorScheme.onBackground,
                        )
                    }
                }
            }

            pagedItems.let { items ->

                val refreshLoadState = items.loadState.refresh
                val isLocalDataAvailable = items.itemCount > 0

                val listState = rememberLazyGridState()

                when {
                    // Show data from database immediately, even if the refresh state is loading
                    refreshLoadState is LoadState.Loading && isLocalDataAvailable -> {
                        MoviesList(
                            pagedItems = pagedItems,
                            spacing = spacing,
                            listState = listState,
                            onMovieClicked = onMovieClicked,
                        )
                    }
                    refreshLoadState is LoadState.Loading -> {
                        LoadingIndicator(modifier = Modifier.fillMaxSize())
                    }
                    refreshLoadState is LoadState.NotLoading &&
                        items.loadState.append.endOfPaginationReached &&
                        items.itemCount == 0 -> {
                        MessageBox(
                            message = stringResource(R.string.no_movies_found),
                            horizontalPadding = spacing.medium,
                            fontSize = fontSizes.medium,
                            color = MaterialTheme.colorScheme.onBackground,
                        )
                    }
                    else -> {
                        MoviesList(
                            pagedItems = pagedItems,
                            spacing = spacing,
                            listState = listState,
                            onMovieClicked = onMovieClicked,
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun MoviesList(
    pagedItems: LazyPagingItems<Movie>,
    spacing: Spacing,
    listState: LazyGridState,
    onMovieClicked: (Int) -> Unit,
) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        state = listState,
        modifier = Modifier.padding(start = spacing.small, end = spacing.small),
    ) {
        item(
            span = { GridItemSpan(2) },
        ) {
            Row(
                modifier =
                    Modifier
                        .fillMaxWidth()
                        .padding(bottom = spacing.medium),
                horizontalArrangement = Arrangement.Absolute.Right,
            ) {
                Text(
                    stringResource(R.string.popular_movies),
                    color = MaterialTheme.colorScheme.onBackground,
                    modifier = Modifier.padding(end = spacing.small),
                )
            }
        }

        pagedItems.let { items ->
            items(count = items.itemCount) { index ->
                val movie = items[index]
                movie?.let {
                    MovieItem(movie = it, onClick = { onMovieClicked(it.id) })
                }
            }
        }
    }
}

@Composable
fun MovieItem(
    movie: Movie,
    onClick: () -> Unit,
) {
    val spacing = MaterialTheme.spacing
    Column(
        modifier =
            Modifier
                .clickable(onClick = onClick)
                .padding(spacing.small),
    ) {
        val validPosterUrl =
            if (movie.moviePosterUrl != "null" && !movie.moviePosterUrl.isNullOrEmpty()) {
                movie.moviePosterUrl
            } else {
                null
            }

        Box(
            modifier =
                Modifier
                    .height(spacing.moviePosterHeight)
                    .clip(RoundedCornerShape(spacing.moviePosterCornerRadius)),
            contentAlignment = Alignment.Center,
        ) {
            AsyncImage(
                model = validPosterUrl ?: R.drawable.movie_placeholder,
                contentDescription = movie.title,
                contentScale = ContentScale.Crop,
                placeholder = painterResource(R.drawable.movie_placeholder),
                error = painterResource(R.drawable.movie_placeholder),
                modifier = Modifier.fillMaxSize(),
            )
        }

        Spacer(modifier = Modifier.height(spacing.small))

        Text(
            text = movie.title?.take(30) ?: "---",
            color = MaterialTheme.colorScheme.onBackground,
            textAlign = TextAlign.Center,
        )

        if (movie.averageVote != null) {
            Row(
                horizontalArrangement = Arrangement.spacedBy(spacing.extraSmall),
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(top = spacing.extraSmall, bottom = spacing.movieItemBottomPadding),
            ) {
                Text(
                    text = String.format(Locale.US, "%.1f", movie.averageVote),
                    color = MaterialTheme.colorScheme.onBackground,
                )
                Icon(
                    imageVector = Icons.Filled.Star,
                    contentDescription = null,
                    tint = StarColor,
                )
            }
        }
    }
}

@Composable
fun SearchFieldWithCloseButton(
    searchText: String,
    showSearchField: Boolean,
    requestFocus: Boolean,
    onSearchAction: (SearchAction) -> Unit,
) {
    val spacing = MaterialTheme.spacing
    Row(verticalAlignment = Alignment.CenterVertically) {
        IconButton(onClick = { onSearchAction(SearchAction.ShowSearchFieldChanged(true)) }) {
            Icon(
                painterResource(id = R.drawable.ic_search),
                contentDescription = stringResource(R.string.content_description_search_movies),
                tint = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.size(spacing.searchIconSize),
            )
        }

        if (showSearchField) {
            SearchTextField(
                searchText = searchText,
                requestFocus = requestFocus,
                onSearchAction = onSearchAction,
            )
        }
    }
}

@Composable
fun SearchTextField(
    searchText: String,
    requestFocus: Boolean,
    onSearchAction: (SearchAction) -> Unit,
) {
    val spacing = MaterialTheme.spacing
    val focusRequester = remember { FocusRequester() }
    val textFieldValue = rememberSaveable { mutableStateOf(searchText) }

    Row(verticalAlignment = Alignment.CenterVertically) {
        val textColor = MaterialTheme.colorScheme.onPrimary
        BasicTextField(
            value = textFieldValue.value,
            onValueChange = { newText ->
                textFieldValue.value = newText
                onSearchAction(SearchAction.SearchTextChanged(newText))
            },
            singleLine = true,
            textStyle = TextStyle(color = textColor, fontSize = MaterialTheme.typography.titleLarge.fontSize),
            cursorBrush = SolidColor(textColor),
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Search),
            modifier =
                Modifier
                    .focusRequester(focusRequester)
                    .weight(1f)
                    .background(MaterialTheme.colorScheme.secondary, shape = MaterialTheme.shapes.small)
                    .height(spacing.searchFieldHeight)
                    .padding(horizontal = spacing.small, vertical = spacing.extraSmall),
        )

        // Only request focus if explicitly asked to (e.g., when the search field is first shown)
        LaunchedEffect(requestFocus) {
            if (requestFocus) {
                focusRequester.requestFocus()
            }
        }

        IconButton(
            onClick = {
                onSearchAction(SearchAction.CloseSearchField)
            },
        ) {
            Icon(
                imageVector = Icons.Default.Close,
                contentDescription = stringResource(R.string.content_description_closing_search_field),
                modifier =
                    Modifier.padding(
                        start = spacing.extraMedium,
                        end = spacing.extraMedium,
                        top = spacing.extraMedium,
                        bottom = spacing.extraMedium,
                    ),
                tint = MaterialTheme.colorScheme.onBackground,
            )
        }
    }
}

@Composable
@DevicePreviews
fun MoviesScreenDarkModePreview() {
    DarkModePreview {
        // Use fake data
        val moviesDetailsList =
            listOf(
                Constants.MOVIE1,
                Constants.MOVIE2,
            )

        // Set fake movies in the repository
        TestMoviesRepositoryImpl.setPopularMovies(moviesDetailsList)

        // Provide the fake ViewModel
        val viewModel = TestMoviesScreenViewModelProvider.provideMoviesScreenViewModel()

        // Display the MoviesScreen with the fake data
        MoviesScreen(
            viewModel = viewModel,
            paddingValues = PaddingValues(),
            onError = {},
            onMovieClicked = {},
        )
    }
}
