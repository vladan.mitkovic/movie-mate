package me.mitkovic.kotlin.moviemate.logging

import timber.log.Timber

class TimberLogger : AppLogger {

    override fun logDebug(message: String) {
        Timber.d(message)
    }

    override fun logDebugWithTag(
        tag: String,
        message: String,
    ) {
        Timber.tag(tag).d(message)
    }

    override fun logError(
        message: String?,
        throwable: Throwable?,
        tag: String?,
    ) {
        Timber.e(throwable, message)
    }
}
