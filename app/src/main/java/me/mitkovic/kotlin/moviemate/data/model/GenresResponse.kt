package me.mitkovic.kotlin.moviemate.data.model

import com.google.gson.annotations.SerializedName

data class GenresResponse(
    @SerializedName("genres")
    val genreResponses: List<GenreResponse>
)
