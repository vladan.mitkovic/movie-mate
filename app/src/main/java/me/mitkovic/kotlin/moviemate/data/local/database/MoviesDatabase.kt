package me.mitkovic.kotlin.moviemate.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import me.mitkovic.kotlin.moviemate.data.local.database.genres.GenresDao
import me.mitkovic.kotlin.moviemate.data.local.database.movies.MoviesDao
import me.mitkovic.kotlin.moviemate.data.model.database.Genre
import me.mitkovic.kotlin.moviemate.data.model.database.Movie

@Database(entities = [Movie::class, Genre::class], version = 1, exportSchema = false)
@TypeConverters(MovieTypesConverter::class)
abstract class MoviesDatabase : RoomDatabase() {

    abstract fun moviesDao(): MoviesDao

    abstract fun genresDao(): GenresDao
}
