package me.mitkovic.kotlin.moviemate.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import me.mitkovic.kotlin.moviemate.R
import me.mitkovic.kotlin.moviemate.common.ConnectivityObserver
import me.mitkovic.kotlin.moviemate.ui.common.HandleSnackbarMessage
import me.mitkovic.kotlin.moviemate.ui.navigation.BottomNavigation
import me.mitkovic.kotlin.moviemate.ui.navigation.MovieMateAppNavigation
import me.mitkovic.kotlin.moviemate.ui.theme.MovieMateTheme
import me.mitkovic.kotlin.moviemate.ui.utils.NavigationUtils.isSelectedRoute
import org.koin.android.ext.android.inject
import org.koin.androidx.compose.koinViewModel

class MainActivity : ComponentActivity() {

    private val connectivityObserver: ConnectivityObserver by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)
        setContent {
            MovieMateApp(connectivityObserver)
        }
    }
}

@Composable
fun MovieMateApp(connectivityObserver: ConnectivityObserver) {
    val viewModel: MainViewModel = koinViewModel()
    val themeValue by viewModel.theme.collectAsState()

    MovieMateTheme(themeValue) {
        val networkStatus by connectivityObserver.observe().collectAsState(
            initial = ConnectivityObserver.Status.Available,
        )

        val snackbarHostState = remember { SnackbarHostState() }
        val errorMessageState = remember { mutableStateOf<Pair<String, Long>?>(null) }

        val navController = rememberNavController()

        Scaffold(
            contentColor = MaterialTheme.colorScheme.background,
            modifier = Modifier.fillMaxSize(),
            snackbarHost = { SnackbarHost(snackbarHostState) },
            bottomBar = {
                Column {
                    if (networkStatus != ConnectivityObserver.Status.Available) {
                        Box(
                            modifier =
                                Modifier
                                    .fillMaxWidth()
                                    .background(Color.Red),
                            contentAlignment = Alignment.Center,
                        ) {
                            Text(text = stringResource(R.string.offline), color = Color.White)
                        }
                    }
                    // Bottom Navigation Bar
                    BottomNavigationBar(
                        navController = navController,
                        onError = { message, throwable ->
                            viewModel.logMessage(message, throwable)
                        },
                    )
                }
            },
        ) { paddingValues ->

            // NavHost
            MovieMateAppNavigation(
                navHostController = navController,
                paddingValues = paddingValues,
                onError = { errorMessage ->
                    errorMessageState.value = errorMessage to System.currentTimeMillis()
                },
            )
        }

        errorMessageState.value?.let { errorMessage ->
            HandleSnackbarMessage(
                snackbarHostState = snackbarHostState,
                snackbarMessage = errorMessage,
            )
        }
    }
}

@Composable
fun BottomNavigationBar(
    navController: NavController,
    onError: (String, Throwable?) -> Unit,
) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute =
        navBackStackEntry?.destination?.route
            ?: BottomNavigation.HOME.route::class.qualifiedName.orEmpty()

    val currentRouteTrimmed by remember(currentRoute) {
        derivedStateOf { currentRoute.substringBefore("?") }
    }

    BottomAppBar(
        containerColor = MaterialTheme.colorScheme.background,
        contentColor = MaterialTheme.colorScheme.onBackground,
    ) {
        BottomNavigation.entries.forEachIndexed { index, navigationItem ->
            NavigationBarItem(
                selected =
                    isSelectedRoute(
                        currentRouteTrimmed,
                        navigationItem.route::class.qualifiedName.toString(),
                        navController,
                    ),
                label = { Text(stringResource(navigationItem.label)) },
                icon = {
                    Icon(
                        painter = painterResource(id = navigationItem.icon),
                        contentDescription = stringResource(navigationItem.label),
                    )
                },
                onClick = {
                    if (navBackStackEntry?.destination?.route !=
                        navigationItem.route::class.qualifiedName.toString()
                    ) {
                        try {
                            navController.navigate(
                                navigationItem.route::class.qualifiedName.toString(),
                            ) {
                                popUpTo(navController.graph.findStartDestination().id) {
                                    inclusive = false
                                }
                                launchSingleTop = true
                                restoreState = true
                            }
                        } catch (e: Exception) {
                            onError("Navigation error: ${e.message}", e)
                        }
                    } else {
                        navController.popBackStack(
                            navigationItem.route::class.qualifiedName.toString(),
                            inclusive = false,
                        )
                    }
                },
            )
        }
    }
}
