package me.mitkovic.kotlin.moviemate.testing.common

import me.mitkovic.kotlin.moviemate.logging.AppLogger

object TestAppLoggerImpl : AppLogger {

    override fun logDebug(message: String) {
        // No-op
    }

    override fun logDebugWithTag(
        tag: String,
        message: String,
    ) {
        // No-op
    }

    override fun logError(
        message: String?,
        throwable: Throwable?,
        tag: String?,
    ) {
        // No-op
    }
}
