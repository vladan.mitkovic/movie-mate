package me.mitkovic.kotlin.moviemate.data.local.datastore

import androidx.appcompat.app.AppCompatDelegate
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ThemeDataSourceImpl(
    private val dataStore: DataStore<Preferences>,
) : ThemeDataSource {

    companion object {
        val THEME_KEY = intPreferencesKey(name = "theme")
    }

    override suspend fun saveTheme(themeValue: Int) {
        dataStore.edit { preferences ->
            preferences[THEME_KEY] = themeValue
        }
    }

    override fun getTheme(): Flow<Int> =
        dataStore.data
            .map { preferences ->
                preferences[THEME_KEY] ?: AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
            }
}
