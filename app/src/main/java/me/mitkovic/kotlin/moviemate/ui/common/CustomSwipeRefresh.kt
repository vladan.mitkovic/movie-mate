package me.mitkovic.kotlin.moviemate.ui.common

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectVerticalDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.unit.IntOffset
import kotlinx.coroutines.launch

/*
*
* CustomSwipeRefresh(
        isRefreshing = isRefreshing,
        onRefresh = { viewModel.fetchFavoriteMovies() }
    ) {
        Column(modifier = Modifier.padding(paddingValues)) {
 *
 * ....
* */

@Composable
fun CustomSwipeRefresh(
    isRefreshing: Boolean,
    onRefresh: () -> Unit,
    content: @Composable () -> Unit,
) {
    var offsetY by remember { mutableFloatStateOf(0f) }
    val scope = rememberCoroutineScope()

    Box(
        modifier =
            Modifier
                .fillMaxSize()
                .pointerInput(Unit) {
                    detectVerticalDragGestures(
                        onVerticalDrag = { change, dragAmount ->
                            change.consume()
                            if (dragAmount > 0) { // Dragging down
                                offsetY += dragAmount
                            }
                        },
                        onDragEnd = {
                            if (offsetY > 100) { // Threshold for refresh
                                scope.launch {
                                    onRefresh()
                                }
                            }
                            offsetY = 0f
                        },
                    )
                },
    ) {
        Box(
            modifier =
                Modifier
                    .fillMaxSize()
                    .offset { IntOffset(0, offsetY.toInt()) },
        ) {
            content()
        }

        // Show CircularProgressIndicator based on offsetY or isRefreshing
        if (offsetY > 0 || isRefreshing) {
            Box(
                modifier =
                    Modifier
                        .fillMaxSize()
                        .background(Color.Black.copy(alpha = 0.5f)),
                // Semi-transparent background
                contentAlignment = Alignment.Center, // Center alignment
            ) {
                CircularProgressIndicator()
            }
        }
    }
}
