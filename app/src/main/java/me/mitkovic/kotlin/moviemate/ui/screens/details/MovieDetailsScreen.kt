package me.mitkovic.kotlin.moviemate.ui.screens.details

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.ArrowBack
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.AsyncImage
import me.mitkovic.kotlin.moviemate.R
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.ui.theme.StarColor
import me.mitkovic.kotlin.moviemate.ui.theme.fontSizes
import me.mitkovic.kotlin.moviemate.ui.theme.spacing
import org.koin.androidx.compose.koinViewModel
import java.util.Locale

@Composable
fun MovieDetailsScreen(
    viewModel: MovieDetailsScreenViewModel = koinViewModel(),
    paddingValues: PaddingValues,
    onError: (String) -> Unit,
    onSimilarMovieClicked: (Int) -> Unit,
    onBackClicked: () -> Unit,
) {
    val spacing = MaterialTheme.spacing
    val fontSizes = MaterialTheme.fontSizes
    val movieDetailsUiState by viewModel.movieDetailsUiState.collectAsStateWithLifecycle()

    // If error happens, post it up the tree until the MainActivity
    // And handle it into Scaffold SnackbarHost
    LaunchedEffect(key1 = movieDetailsUiState.error) {
        movieDetailsUiState.error?.let { message ->
            onError(message)
        }
    }

    Column(modifier = Modifier.padding(paddingValues)) {
        when {
            movieDetailsUiState.isLoading -> {
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center,
                ) {
                    CircularProgressIndicator(color = MaterialTheme.colorScheme.primary)
                }
            }
            movieDetailsUiState.movieDetails != null -> {
                LazyColumn {
                    item {
                        Box(
                            modifier =
                                Modifier
                                    .fillMaxWidth()
                                    .height(spacing.moviePosterHeight),
                        ) {
                            val validPosterUrl =
                                if (movieDetailsUiState.movieDetails?.moviePosterUrl != "null" &&
                                    !movieDetailsUiState.movieDetails?.moviePosterUrl.isNullOrEmpty()
                                ) {
                                    movieDetailsUiState.movieDetails?.moviePosterUrl
                                } else {
                                    null
                                }

                            AsyncImage(
                                model = validPosterUrl ?: R.drawable.movie_placeholder,
                                contentScale = ContentScale.Crop,
                                contentDescription = movieDetailsUiState.movieDetails?.title,
                                placeholder = painterResource(R.drawable.movie_placeholder),
                                error = painterResource(R.drawable.movie_placeholder),
                                modifier = Modifier.height(spacing.moviePosterHeight),
                            )

                            IconButton(
                                onClick = { onBackClicked() },
                                modifier = Modifier.align(Alignment.TopStart).padding(horizontal = spacing.medium),
                            ) {
                                Icon(
                                    imageVector = Icons.AutoMirrored.Rounded.ArrowBack,
                                    contentDescription = null,
                                    tint = Color.White,
                                    modifier = Modifier.size(spacing.xLarge),
                                )
                            }
                        }
                    }
                    item {
                        Spacer(modifier = Modifier.height(spacing.small))

                        Row(
                            modifier =
                                Modifier
                                    .fillMaxWidth()
                                    .padding(horizontal = spacing.medium),
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically,
                        ) {
                            Row(verticalAlignment = Alignment.CenterVertically) {
                                Icon(
                                    imageVector = Icons.Filled.Star,
                                    contentDescription = null,
                                    tint = StarColor,
                                )

                                Text(
                                    text = String.format(Locale.US, "%.1f", movieDetailsUiState.movieDetails?.averageVote),
                                    color = MaterialTheme.colorScheme.onBackground,
                                    modifier = Modifier.padding(start = spacing.extraSmall),
                                )

                                Spacer(modifier = Modifier.width(spacing.extraMedium))

                                VerticalDivider(
                                    modifier =
                                        Modifier
                                            .height(spacing.extraMedium)
                                            .width(spacing.dividerWidth),
                                    color = MaterialTheme.colorScheme.onSurface,
                                )

                                Spacer(modifier = Modifier.width(spacing.extraMedium))

                                Text(
                                    text = "${movieDetailsUiState.movieDetails?.totalVotes}",
                                    color = MaterialTheme.colorScheme.onSurface,
                                )
                            }

                            val isFavorite = movieDetailsUiState.movieDetails?.isFavorite ?: false
                            IconButton(
                                onClick = {
                                    movieDetailsUiState.movieDetails?.id?.let { viewModel.toggleFavoriteStatus(it) }
                                },
                            ) {
                                Icon(
                                    imageVector = if (isFavorite) Icons.Filled.Favorite else Icons.Outlined.FavoriteBorder,
                                    contentDescription = null,
                                    tint = if (isFavorite) Color.Red else MaterialTheme.colorScheme.onSurface,
                                    modifier = Modifier.size(spacing.favoriteIconSize),
                                )
                            }
                        }
                    }
                    item {
                        Spacer(modifier = Modifier.height(spacing.large))

                        movieDetailsUiState.movieDetails?.title?.let { title ->
                            Text(
                                text = title,
                                color = MaterialTheme.colorScheme.onBackground,
                                fontSize = fontSizes.medium,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier.padding(start = spacing.medium),
                            )
                        }
                    }

                    item {
                        Spacer(modifier = Modifier.height(spacing.medium))

                        Column(
                            modifier =
                                Modifier
                                    .fillMaxWidth()
                                    .padding(horizontal = spacing.medium)
                                    .wrapContentSize(Alignment.CenterStart),
                        ) {
                            movieDetailsUiState.movieDetails?.genreIds?.let { genres ->
                                Text(
                                    text = genres,
                                    color = MaterialTheme.colorScheme.onSurface,
                                    fontSize = fontSizes.small,
                                )
                            }

                            Spacer(modifier = Modifier.width(spacing.extraSmall))

                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                            ) {
                                Box(
                                    modifier =
                                        Modifier
                                            .size(spacing.genreDotSize)
                                            .clip(CircleShape)
                                            .background(MaterialTheme.colorScheme.onSurface),
                                )

                                Spacer(modifier = Modifier.width(spacing.extraSmall))

                                movieDetailsUiState.movieDetails?.releaseDate?.let { year ->
                                    Text(
                                        text = year,
                                        color = MaterialTheme.colorScheme.onSurface,
                                        fontSize = fontSizes.small,
                                    )
                                }
                            }
                        }
                    }

                    item {
                        Spacer(modifier = Modifier.height(spacing.large))

                        movieDetailsUiState.movieDetails?.overview?.let { overview ->
                            Text(
                                text = overview,
                                color = MaterialTheme.colorScheme.onBackground,
                                fontSize = fontSizes.small,
                                textAlign = TextAlign.Start,
                                modifier = Modifier.padding(start = spacing.medium, end = spacing.medium),
                            )
                        }
                    }
                    if (movieDetailsUiState.similarMovies.movies.isNotEmpty()) {
                        item {
                            Spacer(modifier = Modifier.height(spacing.small))
                            Text(
                                text = stringResource(R.string.similar_movies),
                                color = MaterialTheme.colorScheme.onBackground,
                                fontSize = fontSizes.medium,
                                modifier = Modifier.padding(start = spacing.medium, top = spacing.medium),
                            )
                            Spacer(modifier = Modifier.height(spacing.extraSmall))
                            LazyRow(
                                horizontalArrangement = Arrangement.spacedBy(spacing.extraSmall),
                                modifier = Modifier.padding(horizontal = spacing.medium),
                            ) {
                                items(movieDetailsUiState.similarMovies.movies) { similarMovie ->
                                    SimilarMovieItem(movie = similarMovie, onClick = { onSimilarMovieClicked(similarMovie.id) })
                                }
                            }
                        }
                    }
                }
            }
            movieDetailsUiState.movieDetails == null -> {
                Box(
                    modifier =
                        Modifier
                            .fillMaxWidth()
                            .height(spacing.moviePosterHeight),
                ) {
                    IconButton(
                        onClick = { onBackClicked() },
                        modifier = Modifier.align(Alignment.TopStart).padding(horizontal = spacing.medium),
                    ) {
                        Icon(
                            imageVector = Icons.AutoMirrored.Rounded.ArrowBack,
                            contentDescription = null,
                            tint = MaterialTheme.colorScheme.onBackground,
                            modifier = Modifier.size(spacing.xLarge),
                        )
                    }

                    Text(
                        text = stringResource(id = R.string.no_movie_data),
                        color = MaterialTheme.colorScheme.onBackground,
                        fontSize = fontSizes.medium,
                        modifier =
                            Modifier
                                .align(Alignment.BottomStart)
                                .padding(start = spacing.medium),
                    )
                }
            }
        }
    }
}

@Composable
fun SimilarMovieItem(
    movie: Movie,
    onClick: () -> Unit,
) {
    val spacing = MaterialTheme.spacing
    Column(
        modifier =
            Modifier
                .clickable(onClick = onClick)
                .padding(spacing.small),
    ) {
        val validPosterUrl =
            if (movie.moviePosterUrl != "null" && !movie.moviePosterUrl.isNullOrEmpty()) {
                movie.moviePosterUrl
            } else {
                null
            }

        AsyncImage(
            model = validPosterUrl ?: R.drawable.movie_placeholder,
            contentDescription = null,
            contentScale = ContentScale.Crop,
            placeholder = painterResource(R.drawable.movie_placeholder),
            error = painterResource(R.drawable.movie_placeholder),
            modifier =
                Modifier
                    .widthIn(max = spacing.moviePosterWidth)
                    .height(spacing.moviePosterHeight)
                    .clip(RoundedCornerShape(spacing.moviePosterCornerRadius)),
        )

        Spacer(modifier = Modifier.height(spacing.small))

        val title =
            movie.title?.let {
                if (it.length > 20) "${it.take(20)}..." else it
            } ?: "---"

        Text(
            text = title,
            color = MaterialTheme.colorScheme.onBackground,
            textAlign = TextAlign.Center,
        )

        if (movie.averageVote != null) {
            Row(
                horizontalArrangement = Arrangement.spacedBy(spacing.extraSmall),
                modifier =
                    Modifier.padding(
                        top = spacing.extraSmall,
                        bottom = spacing.movieItemBottomPadding,
                    ),
            ) {
                Text(
                    text = String.format(Locale.US, "%.1f", movie.averageVote),
                    color = MaterialTheme.colorScheme.onBackground,
                )

                Icon(
                    imageVector = Icons.Filled.Star,
                    contentDescription = null,
                    tint = StarColor,
                )
            }
        }
    }
}
