package me.mitkovic.kotlin.moviemate.data.local.database.genres

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.moviemate.data.model.database.Genre

@Dao
interface GenresDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGenres(genres: List<Genre>)

    @Query("SELECT * FROM genres WHERE id = :id")
    suspend fun getGenreById(id: Int): Genre?

    @Query("SELECT * FROM genres")
    fun getAllGenres(): Flow<List<Genre>>
}
