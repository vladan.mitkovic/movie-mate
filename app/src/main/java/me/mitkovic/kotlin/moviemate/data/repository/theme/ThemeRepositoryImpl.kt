package me.mitkovic.kotlin.moviemate.data.repository.theme

import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.moviemate.data.local.LocalDataSource

class ThemeRepositoryImpl(
    private val localDataSource: LocalDataSource,
) : ThemeRepository {

    override suspend fun saveTheme(themeValue: Int) =
        localDataSource
            .themeDataSource
            .saveTheme(themeValue)

    override fun getTheme(): Flow<Int> =
        localDataSource
            .themeDataSource
            .getTheme()
}
