package me.mitkovic.kotlin.moviemate.data.local

import me.mitkovic.kotlin.moviemate.data.local.database.genres.GenresDataSource
import me.mitkovic.kotlin.moviemate.data.local.database.movies.MoviesDataSource
import me.mitkovic.kotlin.moviemate.data.local.datastore.ThemeDataSource

class LocalDataSourceImpl(
    override val moviesDataSource: MoviesDataSource,
    override val genresDataSource: GenresDataSource,
    override val themeDataSource: ThemeDataSource,
) : LocalDataSource
