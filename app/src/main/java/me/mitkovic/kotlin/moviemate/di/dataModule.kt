package me.mitkovic.kotlin.moviemate.di

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStoreFile
import androidx.room.Room
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.data.local.LocalDataSource
import me.mitkovic.kotlin.moviemate.data.local.LocalDataSourceImpl
import me.mitkovic.kotlin.moviemate.data.local.database.MoviesDatabase
import me.mitkovic.kotlin.moviemate.data.local.database.genres.GenresDao
import me.mitkovic.kotlin.moviemate.data.local.database.genres.GenresDataSource
import me.mitkovic.kotlin.moviemate.data.local.database.genres.GenresDataSourceImpl
import me.mitkovic.kotlin.moviemate.data.local.database.movies.MoviesDao
import me.mitkovic.kotlin.moviemate.data.local.database.movies.MoviesDataSource
import me.mitkovic.kotlin.moviemate.data.local.database.movies.MoviesDataSourceImpl
import me.mitkovic.kotlin.moviemate.data.local.datastore.ThemeDataSource
import me.mitkovic.kotlin.moviemate.data.local.datastore.ThemeDataSourceImpl
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

val dataModule: Module =
    module {

        // **Database**
        single<MoviesDatabase> {
            Room
                .databaseBuilder(
                    androidContext(),
                    MoviesDatabase::class.java,
                    "movies",
                ).fallbackToDestructiveMigration()
                .build()
        }

        single<MoviesDao> { get<MoviesDatabase>().moviesDao() }
        single<GenresDao> { get<MoviesDatabase>().genresDao() }

        // **Local Data Source**
        single<MoviesDataSource> {
            MoviesDataSourceImpl(
                moviesDao = get<MoviesDao>(),
            )
        }

        single<GenresDataSource> {
            GenresDataSourceImpl(
                genresDao = get<GenresDao>(),
            )
        }

        single<ThemeDataSource> {
            ThemeDataSourceImpl(
                dataStore = get<DataStore<Preferences>>(),
            )
        }

        single<LocalDataSource> {
            LocalDataSourceImpl(
                moviesDataSource = get<MoviesDataSource>(),
                genresDataSource = get<GenresDataSource>(),
                themeDataSource = get<ThemeDataSource>(),
            )
        }

        // **DataStore**
        single<DataStore<Preferences>> {
            PreferenceDataStoreFactory.create(
                produceFile = { androidContext().preferencesDataStoreFile(Constants.MOVIES_PREFERENCES_NAME) },
            )
        }
    }
