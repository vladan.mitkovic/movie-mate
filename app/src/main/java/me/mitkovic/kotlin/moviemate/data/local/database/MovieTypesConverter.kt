package me.mitkovic.kotlin.moviemate.data.local.database

import androidx.room.TypeConverter

class MovieTypesConverter {

    @TypeConverter
    fun fromListToString(list: List<Int>?): String? =
        list
            ?.joinToString(",")

    @TypeConverter
    fun fromStringToList(value: String?): List<Int>? =
        value
            ?.split(",")
            ?.filter { it.isNotBlank() }
            ?.map { it.toInt() }
}
