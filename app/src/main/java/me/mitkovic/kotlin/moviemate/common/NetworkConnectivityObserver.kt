package me.mitkovic.kotlin.moviemate.common

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.distinctUntilChanged

class NetworkConnectivityObserver(
    context: Context,
) : ConnectivityObserver {

    private val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    override fun observe(): Flow<ConnectivityObserver.Status> =
        callbackFlow {
            trySend(getCurrentStatus()).isSuccess

            val callback =
                object : ConnectivityManager.NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        trySend(ConnectivityObserver.Status.Available).isSuccess
                    }

                    override fun onUnavailable() {
                        trySend(ConnectivityObserver.Status.UnAvailable).isSuccess
                    }

                    override fun onLost(network: Network) {
                        trySend(ConnectivityObserver.Status.Lost).isSuccess
                    }

                    override fun onCapabilitiesChanged(
                        network: Network,
                        networkCapabilities: NetworkCapabilities,
                    ) {
                        val status =
                            if (networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
                                ConnectivityObserver.Status.Available
                            } else {
                                ConnectivityObserver.Status.UnAvailable
                            }
                        trySend(status).isSuccess
                    }
                }

            // Register the network callback after emitting current status
            connectivityManager.registerDefaultNetworkCallback(callback)

            // Await closure to unregister the callback
            awaitClose {
                connectivityManager.unregisterNetworkCallback(callback)
            }
        }.distinctUntilChanged()

    private fun getCurrentStatus(): ConnectivityObserver.Status {
        val activeNetwork: Network? = connectivityManager.activeNetwork
        val networkCapabilities: NetworkCapabilities? =
            connectivityManager.getNetworkCapabilities(activeNetwork)
        return if (networkCapabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) == true) {
            ConnectivityObserver.Status.Available
        } else {
            ConnectivityObserver.Status.UnAvailable
        }
    }
}
