package me.mitkovic.kotlin.moviemate.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.data.remote.ApiService
import me.mitkovic.kotlin.moviemate.data.remote.RemoteDataSource
import me.mitkovic.kotlin.moviemate.data.remote.RemoteDataSourceImpl
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule =
    module {

        // **Gson**
        single<Gson> { GsonBuilder().create() }

        // **Retrofit**
        single<Retrofit> {
            Retrofit
                .Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(get()))
                .build()
        }

        // **ApiService**
        single<ApiService> { get<Retrofit>().create(ApiService::class.java) }

        // **Remote Data Source**
        single<RemoteDataSource> { RemoteDataSourceImpl(get<ApiService>()) }
    }
