package me.mitkovic.kotlin.moviemate.ui.navigation

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import me.mitkovic.kotlin.moviemate.R

enum class BottomNavigation(
    @StringRes val label: Int,
    @DrawableRes val icon: Int,
    val route: Destination,
) {
    HOME(R.string.home, R.drawable.ic_home, Destination.Home),
    FAVORITES(R.string.favorites, R.drawable.ic_star, Destination.Favorites),
    SETTINGS(R.string.settings, R.drawable.ic_settings, Destination.Settings),
}
