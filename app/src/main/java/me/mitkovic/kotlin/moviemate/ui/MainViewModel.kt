package me.mitkovic.kotlin.moviemate.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import me.mitkovic.kotlin.moviemate.data.repository.MovieMateRepository
import me.mitkovic.kotlin.moviemate.logging.AppLogger
import me.mitkovic.kotlin.moviemate.ui.theme.Theme

class MainViewModel(
    movieMateRepository: MovieMateRepository,
    private val logger: AppLogger,
) : ViewModel() {

    val theme =
        movieMateRepository
            .themeRepository
            .getTheme()
            .stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5_000),
                initialValue = Theme.DARK_THEME.themeValue,
            )

    fun logMessage(
        message: String,
        throwable: Throwable?,
    ) {
        logger.logError(message, throwable)
    }
}
