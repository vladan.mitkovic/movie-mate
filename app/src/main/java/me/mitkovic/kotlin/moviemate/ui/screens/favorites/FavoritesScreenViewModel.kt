package me.mitkovic.kotlin.moviemate.ui.screens.favorites

import androidx.compose.runtime.Immutable
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.data.repository.MovieMateRepository
import me.mitkovic.kotlin.moviemate.logging.AppLogger
import me.mitkovic.kotlin.moviemate.ui.utils.NumberUtils.roundOffDecimal
import me.mitkovic.kotlin.moviemate.ui.utils.stateInWhileSubscribed

data class FavoriteMoviesUiState(
    val isLoading: Boolean = false,
    val favoriteMovies: FavoriteMovies = FavoriteMovies(emptyList()),
    val error: String? = null,
)

@Immutable
data class FavoriteMovies(
    val favoriteMovies: List<Movie>,
)

class FavoritesScreenViewModel(
    movieMateRepository: MovieMateRepository,
    private val logger: AppLogger,
) : ViewModel() {

    val favoriteMoviesUiState: StateFlow<FavoriteMoviesUiState> =
        movieMateRepository
            .moviesRepository
            .getFavoriteMovies()
            .onStart {
                FavoriteMoviesUiState(isLoading = true, error = null)
            }.map { result ->
                when (result) {
                    is Resource.Success -> {
                        val favoriteMovies =
                            result.data?.results?.map { movie ->
                                movie.copy(
                                    averageVote = movie.averageVote?.roundOffDecimal(),
                                    moviePosterUrl = "${Constants.MOVIE_POSTER_URL}${movie.moviePosterUrl}",
                                )
                            } ?: emptyList()
                        FavoriteMoviesUiState(
                            isLoading = false,
                            favoriteMovies = FavoriteMovies(favoriteMovies),
                            error = null,
                        )
                    }
                    is Resource.Error -> {
                        result.message?.let { exception ->
                            logger.logError(result.message, Exception(exception))
                        }
                        FavoriteMoviesUiState(
                            isLoading = false,
                            error = result.message ?: Constants.SOMETHING_WENT_WRONG,
                        )
                    }
                    else -> {
                        FavoriteMoviesUiState()
                    }
                }
            }.catch { e ->
                logger.logError(e.message, e)
                FavoriteMoviesUiState(
                    isLoading = false,
                    error = e.message ?: Constants.SOMETHING_WENT_WRONG,
                )
            }.stateInWhileSubscribed(
                viewModel = this,
                initialValue = FavoriteMoviesUiState(),
            )
}
