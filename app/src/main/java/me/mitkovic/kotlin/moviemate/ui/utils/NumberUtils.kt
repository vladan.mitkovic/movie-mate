package me.mitkovic.kotlin.moviemate.ui.utils

import java.math.RoundingMode
import java.text.DecimalFormat

object NumberUtils {

    fun Double.roundOffDecimal(): Double {
        val decimalFormat = DecimalFormat("#.#")

        decimalFormat.roundingMode = RoundingMode.CEILING

        return decimalFormat.format(this).toDouble()
    }
}
