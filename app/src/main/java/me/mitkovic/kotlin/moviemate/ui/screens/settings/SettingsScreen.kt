package me.mitkovic.kotlin.moviemate.ui.screens.settings

import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.ArrowBack
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import me.mitkovic.kotlin.moviemate.R
import me.mitkovic.kotlin.moviemate.ui.theme.Theme
import me.mitkovic.kotlin.moviemate.ui.theme.spacing
import me.mitkovic.kotlin.moviemate.ui.utils.getAppVersionCode
import me.mitkovic.kotlin.moviemate.ui.utils.getAppVersionName
import org.koin.androidx.compose.koinViewModel

@Composable
fun SettingsScreen(
    viewModel: SettingsScreenViewModel = koinViewModel(),
    paddingValues: PaddingValues,
    onBackClicked: () -> Unit,
) {
    val spacing = MaterialTheme.spacing
    val context = LocalContext.current

    val currentTheme by viewModel.theme.collectAsStateWithLifecycle()

    var showAboutDialog by rememberSaveable { mutableStateOf(false) }
    var showSocialsDialog by rememberSaveable { mutableStateOf(false) }

    Column(
        modifier =
            Modifier
                .padding(paddingValues)
                .background(MaterialTheme.colorScheme.background),
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(spacing.small),
        ) {
            IconButton(
                onClick = { onBackClicked() },
                modifier = Modifier,
            ) {
                Icon(
                    imageVector = Icons.AutoMirrored.Rounded.ArrowBack,
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.onBackground,
                    modifier = Modifier.size(spacing.xLarge),
                )
            }
            Text(
                stringResource(R.string.settings),
                modifier = Modifier.padding(start = spacing.small),
                style = MaterialTheme.typography.headlineLarge,
                color = MaterialTheme.colorScheme.onBackground,
            )
        }

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier =
                Modifier
                    .fillMaxWidth()
                    .padding(spacing.medium)
                    .clickable {
                        val newTheme =
                            if (currentTheme == Theme.DARK_THEME.themeValue) {
                                Theme.LIGHT_THEME.themeValue
                            } else {
                                Theme.DARK_THEME.themeValue
                            }
                        viewModel.updateTheme(newTheme)
                    },
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_theme),
                contentDescription = null,
                tint = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.size(spacing.large),
            )

            Text(
                stringResource(R.string.change_theme),
                modifier = Modifier.padding(start = spacing.small),
                color = MaterialTheme.colorScheme.onBackground,
            )
        }

        HorizontalDivider(
            modifier = Modifier.padding(start = spacing.medium, end = spacing.medium),
            color = MaterialTheme.colorScheme.onSurface,
        )

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier =
                Modifier
                    .fillMaxWidth()
                    .padding(spacing.medium)
                    .clickable {
                        showAboutDialog = true
                    },
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_danger_circle),
                contentDescription = null,
                tint = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.size(spacing.large),
            )

            Text(
                stringResource(R.string.about_title),
                modifier = Modifier.padding(start = spacing.small),
                color = MaterialTheme.colorScheme.onBackground,
            )
        }

        HorizontalDivider(
            modifier = Modifier.padding(start = spacing.medium, end = spacing.medium),
            color = MaterialTheme.colorScheme.onSurface,
        )

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier =
                Modifier
                    .fillMaxWidth()
                    .padding(spacing.medium)
                    .clickable {
                        try {
                            val rateIntent =
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("market://details?id=me.mitkovic.guesswhat"),
                                )
                            context.startActivity(rateIntent, null)
                        } catch (e: Exception) {
                            Toast
                                .makeText(
                                    context,
                                    "Unable to open PlayStore",
                                    Toast.LENGTH_SHORT,
                                ).show()
                        }
                    },
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_star),
                contentDescription = null,
                tint = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.size(spacing.large),
            )

            Text(
                stringResource(R.string.rate_us),
                modifier = Modifier.padding(start = spacing.small),
                color = MaterialTheme.colorScheme.onBackground,
            )
        }

        HorizontalDivider(
            modifier = Modifier.padding(start = spacing.medium, end = spacing.medium),
            color = MaterialTheme.colorScheme.onSurface,
        )

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier =
                Modifier
                    .fillMaxWidth()
                    .padding(spacing.medium)
                    .clickable {
                        try {
                            val sendIntent = Intent()
                            sendIntent.action = Intent.ACTION_SEND
                            sendIntent.putExtra(
                                Intent.EXTRA_TEXT,
                                "Check out GuessWhat App on Playstore: https://play.google.com/store/apps/details?id=me.mitkovic.guesswhat",
                            )
                            sendIntent.type = "text/plain"
                            context.startActivity(sendIntent)
                        } catch (e: Exception) {
                            Toast
                                .makeText(
                                    context,
                                    "Unable to share the app",
                                    Toast.LENGTH_SHORT,
                                ).show()
                        }
                    },
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_share),
                contentDescription = null,
                tint = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.size(spacing.large),
            )

            Text(
                stringResource(R.string.share),
                modifier = Modifier.padding(start = spacing.small),
                color = MaterialTheme.colorScheme.onBackground,
            )
        }

        HorizontalDivider(
            modifier = Modifier.padding(start = spacing.medium, end = spacing.medium),
            color = MaterialTheme.colorScheme.onSurface,
        )

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier =
                Modifier
                    .fillMaxWidth()
                    .padding(spacing.medium)
                    .clickable {
                        showSocialsDialog = true
                    },
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_socials),
                contentDescription = null,
                tint = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.size(spacing.large),
            )

            Text(
                stringResource(R.string.get_in_touch),
                modifier = Modifier.padding(start = spacing.small),
                color = MaterialTheme.colorScheme.onBackground,
            )
        }

        HorizontalDivider(
            modifier = Modifier.padding(start = spacing.medium, end = spacing.medium),
            color = MaterialTheme.colorScheme.onSurface,
        )
    }

    if (showAboutDialog) {
        AboutDialog(
            onDismiss = {
                showAboutDialog = false
            },
        )
    }

    if (showSocialsDialog) {
        GetInTouchDialog(
            onDismiss = {
                showSocialsDialog = false
            },
        )
    }
}

@Composable
private fun AboutDialog(onDismiss: () -> Unit) {
    AlertDialog(
        modifier =
            Modifier
                .fillMaxWidth()
                .padding(10.dp),
        onDismissRequest = onDismiss,
        tonalElevation = 0.dp,
        containerColor = MaterialTheme.colorScheme.background,
        title = {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = "About",
                textAlign = TextAlign.Center,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.onBackground,
            )
        },
        text = {
            Column(
                modifier =
                    Modifier
                        .fillMaxWidth(),
                verticalArrangement = Arrangement.Center,
            ) {
                Row(
                    modifier =
                        Modifier
                            .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                    val context = LocalContext.current
                    LazyColumn(
                        contentPadding = PaddingValues(16.dp),
                    ) {
                        item {
                            Text(
                                modifier = Modifier.fillMaxWidth(),
                                text = stringResource(id = R.string.app_name),
                                style = MaterialTheme.typography.headlineLarge,
                                color = MaterialTheme.colorScheme.onBackground,
                                textAlign = TextAlign.Center,
                            )
                        }

                        item {
                            Spacer(modifier = Modifier.height(8.dp))
                            Text(
                                modifier =
                                    Modifier
                                        .fillMaxWidth()
                                        .padding(16.dp),
                                text = stringResource(R.string.about),
                                style = MaterialTheme.typography.bodySmall,
                                color = MaterialTheme.colorScheme.onBackground,
                                textAlign = TextAlign.Center,
                            )
                        }

                        item {
                            Text(
                                modifier = Modifier.fillMaxWidth(),
                                text =
                                    stringResource(
                                        R.string.v,
                                        context.getAppVersionName(),
                                        context.getAppVersionCode(),
                                    ),
                                style = MaterialTheme.typography.bodyMedium,
                                color = MaterialTheme.colorScheme.onBackground,
                                textAlign = TextAlign.Center,
                            )
                        }
                    }
                }
            }
        },
        confirmButton = {
            Button(
                onClick = onDismiss,
                colors =
                    ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.primary,
                        contentColor = MaterialTheme.colorScheme.onPrimary,
                    ),
            ) {
                Text(
                    text = "Okay",
                )
            }
        },
        shape = RoundedCornerShape(10.dp),
    )
}

@Composable
private fun GetInTouchDialog(onDismiss: () -> Unit) {
    val context = LocalContext.current
    AlertDialog(
        modifier =
            Modifier
                .fillMaxWidth()
                .padding(10.dp),
        onDismissRequest = onDismiss,
        tonalElevation = 0.dp,
        containerColor = MaterialTheme.colorScheme.background,
        title = {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = "Get in touch",
                textAlign = TextAlign.Center,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.onBackground,
            )
        },
        text = {
            Column(
                modifier =
                    Modifier
                        .fillMaxWidth(),
                verticalArrangement = Arrangement.Center,
            ) {
                Row(
                    modifier =
                        Modifier
                            .fillMaxWidth()
                            .clickable {
                                val intent = Intent(Intent.ACTION_VIEW)
                                intent.data =
                                    Uri.parse("https://www.linkedin.com/in/vladanmitkovic/")
                                context.startActivity(intent, null)
                            },
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceEvenly,
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic__linkedin),
                            tint = MaterialTheme.colorScheme.onBackground.copy(.5f),
                            contentDescription = null,
                        )
                        Spacer(modifier = Modifier.width(8.dp))
                        Text(
                            text = "Linkedin",
                            textAlign = TextAlign.Left,
                            fontSize = 18.sp,
                            fontWeight = FontWeight.SemiBold,
                            color = MaterialTheme.colorScheme.onBackground,
                        )
                    }
                    Icon(
                        painter = painterResource(id = R.drawable.ic_chevron_right),
                        tint = MaterialTheme.colorScheme.onBackground.copy(.5f),
                        contentDescription = null,
                    )
                }
                Spacer(modifier = Modifier.height(24.dp))

                Row(
                    modifier =
                        Modifier
                            .fillMaxWidth()
                            .clickable {
                                Toast
                                    .makeText(context, "GitLab", Toast.LENGTH_SHORT)
                                    .show()

                                val intent = Intent(Intent.ACTION_VIEW)
                                intent.data =
                                    Uri.parse("https://gitlab.com/vladan.mitkovic/")
                                context.startActivity(intent, null)
                            },
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceEvenly,
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_github),
                            tint = MaterialTheme.colorScheme.onBackground.copy(.5f),
                            contentDescription = null,
                        )
                        Spacer(modifier = Modifier.width(8.dp))
                        Text(
                            text = "GitLab",
                            textAlign = TextAlign.Left,
                            fontSize = 18.sp,
                            fontWeight = FontWeight.SemiBold,
                            color = MaterialTheme.colorScheme.onBackground,
                        )
                    }
                    Icon(
                        painter = painterResource(id = R.drawable.ic_chevron_right),
                        tint = MaterialTheme.colorScheme.onBackground.copy(.5f),
                        contentDescription = null,
                    )
                }
            }
        },
        confirmButton = {
            Button(
                onClick = onDismiss,
                colors =
                    ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.primary,
                        contentColor = MaterialTheme.colorScheme.onPrimary,
                    ),
            ) {
                Text(
                    text = "Okay",
                )
            }
        },
        shape = RoundedCornerShape(10.dp),
    )
}
