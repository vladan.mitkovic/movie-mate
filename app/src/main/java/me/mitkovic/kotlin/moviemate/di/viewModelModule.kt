package me.mitkovic.kotlin.moviemate.di

import me.mitkovic.kotlin.moviemate.data.repository.MovieMateRepository
import me.mitkovic.kotlin.moviemate.logging.AppLogger
import me.mitkovic.kotlin.moviemate.ui.MainViewModel
import me.mitkovic.kotlin.moviemate.ui.screens.details.MovieDetailsScreenViewModel
import me.mitkovic.kotlin.moviemate.ui.screens.favorites.FavoritesScreenViewModel
import me.mitkovic.kotlin.moviemate.ui.screens.home.MoviesScreenViewModel
import me.mitkovic.kotlin.moviemate.ui.screens.settings.SettingsScreenViewModel
import org.koin.core.module.dsl.viewModel
import org.koin.dsl.module

val viewModelModule =
    module {

        viewModel {
            MainViewModel(
                movieMateRepository = get<MovieMateRepository>(),
                logger = get<AppLogger>(),
            )
        }

        viewModel {
            MoviesScreenViewModel(
                movieMateRepository = get<MovieMateRepository>(),
                logger = get<AppLogger>(),
            )
        }

        viewModel {
            FavoritesScreenViewModel(
                movieMateRepository = get<MovieMateRepository>(),
                logger = get<AppLogger>(),
            )
        }

        viewModel {
            SettingsScreenViewModel(
                movieMateRepository = get<MovieMateRepository>(),
            )
        }

        viewModel {
            MovieDetailsScreenViewModel(
                movieMateRepository = get<MovieMateRepository>(),
                logger = get<AppLogger>(),
                savedStateHandle = get(),
            )
        }
    }
