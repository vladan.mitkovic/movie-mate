package me.mitkovic.kotlin.moviemate.data.local

import me.mitkovic.kotlin.moviemate.data.local.database.genres.GenresDataSource
import me.mitkovic.kotlin.moviemate.data.local.database.movies.MoviesDataSource
import me.mitkovic.kotlin.moviemate.data.local.datastore.ThemeDataSource

interface LocalDataSource {
    val moviesDataSource: MoviesDataSource
    val genresDataSource: GenresDataSource
    val themeDataSource: ThemeDataSource
}
