package me.mitkovic.kotlin.moviemate.testing.data.repository

import me.mitkovic.kotlin.moviemate.data.repository.MovieMateRepository
import me.mitkovic.kotlin.moviemate.data.repository.genres.GenresRepository
import me.mitkovic.kotlin.moviemate.data.repository.movies.MoviesRepository
import me.mitkovic.kotlin.moviemate.data.repository.theme.ThemeRepository
import me.mitkovic.kotlin.moviemate.testing.data.repository.genres.TestGenresRepositoryImpl
import me.mitkovic.kotlin.moviemate.testing.data.repository.movies.TestMoviesRepositoryImpl
import me.mitkovic.kotlin.moviemate.testing.data.repository.theme.TestThemeRepositoryImpl

object TestMovieMateRepositoryImpl : MovieMateRepository {
    override val moviesRepository: MoviesRepository = TestMoviesRepositoryImpl
    override val genresRepository: GenresRepository = TestGenresRepositoryImpl
    override val themeRepository: ThemeRepository = TestThemeRepositoryImpl
}
