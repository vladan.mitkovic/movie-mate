package me.mitkovic.kotlin.moviemate.testing.data.repository.movies

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import me.mitkovic.kotlin.moviemate.data.model.MoviesResponse
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.data.repository.movies.MoviesRepository
import me.mitkovic.kotlin.moviemate.testing.data.common.TestPagingSource

object TestMoviesRepositoryImpl : MoviesRepository {

    private var movies: List<Movie> = emptyList()
    private var searchResults: List<Movie> = emptyList()
    private var movieDetails: Map<Int, Movie> = emptyMap()
    private var similarMovies: Map<Int, List<Movie>> = emptyMap()

    fun setPopularMovies(movies: List<Movie>) {
        this.movies = movies
        this.movieDetails = movies.associateBy { it.id }
    }

    fun setSearchResults(results: List<Movie>) {
        this.searchResults = results
    }

    fun setMovieDetails(details: Map<Int, Movie>) {
        this.movieDetails = details
    }

    fun setSimilarMovies(similar: Map<Int, List<Movie>>) {
        this.similarMovies = similar
    }

    override fun getPopularMovies(onRemoteError: (Throwable) -> Unit): Flow<PagingData<Movie>> =
        Pager(
            config = PagingConfig(pageSize = 20, enablePlaceholders = false),
            pagingSourceFactory = { TestPagingSource(movies) },
        ).flow

    override fun getMovieDetails(movieId: Int): Flow<Resource<Movie>> =
        flow {
            emit(Resource.Loading())
            val details = movieDetails[movieId]
            if (details != null) {
                emit(Resource.Success(details))
            } else {
                emit(Resource.Error(message = "Movie not found"))
            }
        }

    override fun getSimilarMovies(movieId: Int): Flow<Resource<MoviesResponse>> =
        flow {
            emit(Resource.Loading())
            val similar = similarMovies[movieId] ?: emptyList()
            emit(Resource.Success(MoviesResponse(1, similar)))
        }

    override suspend fun searchMovies(name: String): Flow<PagingData<Movie>> {
        val filteredResults = searchResults.filter { it.title?.contains(name, ignoreCase = true) == true }
        return Pager(
            config =
                PagingConfig(
                    pageSize = 20,
                    enablePlaceholders = false,
                ),
            pagingSourceFactory = { TestPagingSource(filteredResults) },
        ).flow
    }

    override fun getFavoriteMovies(): Flow<Resource<MoviesResponse>> =
        flow {
            emit(Resource.Loading())
            val favoriteMovies = movies.filter { it.isFavorite }
            emit(Resource.Success(MoviesResponse(1, favoriteMovies)))
        }

    override suspend fun updateMovieFavoriteStatus(
        movieId: Int,
        favorite: Boolean,
    ) {
        movies =
            movies.map { movie ->
                if (movie.id == movieId) movie.copy(isFavorite = favorite) else movie
            }
    }

    override suspend fun isFavorite(movieId: Int): Boolean = movies.any { it.id == movieId && it.isFavorite }
}
