package me.mitkovic.kotlin.moviemate.ui.screens.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import me.mitkovic.kotlin.moviemate.data.repository.MovieMateRepository
import me.mitkovic.kotlin.moviemate.ui.theme.Theme

class SettingsScreenViewModel(
    private val movieMateRepository: MovieMateRepository,
) : ViewModel() {

    val theme =
        movieMateRepository
            .themeRepository
            .getTheme()
            .stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5_000),
                initialValue = Theme.DARK_THEME.themeValue,
            )

    fun updateTheme(themeValue: Int) {
        viewModelScope.launch {
            movieMateRepository
                .themeRepository
                .saveTheme(themeValue)
        }
    }
}
