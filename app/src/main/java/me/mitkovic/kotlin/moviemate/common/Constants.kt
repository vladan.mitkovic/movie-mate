package me.mitkovic.kotlin.moviemate.common

import me.mitkovic.kotlin.moviemate.data.model.GenreResponse
import me.mitkovic.kotlin.moviemate.data.model.GenresResponse
import me.mitkovic.kotlin.moviemate.data.model.database.Movie

object Constants {

    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val PAGE_SIZE = 20
    const val MOVIE_POSTER_URL = "https://image.tmdb.org/t/p/w220_and_h330_face/"

    // API_KEY will be read from ENV (environment) variables on the local computer.
    const val API_KEY = "8bf0b880d9a8021c8f0dc895e346f252"

    const val MOVIES_PREFERENCES_NAME = "movies_preferences"

    const val UNKNOWN_ERROR_OCCURRED = "Unknown Error Occurred"
    const val SOMETHING_WENT_WRONG = "Something went wrong!"
    const val UNKNOWN = "---"

    val MOVIE1 =
        Movie(
            id = 1,
            title = "Movie 1",
            averageVote = 8.5,
            moviePosterUrl = "url1",
            totalVotes = 100,
            releaseDate = "2020",
            overview = "Overview 1",
            genreIds = listOf(1, 2, 3),
            order = 1,
        )

    val MOVIE2 =
        Movie(
            id = 2,
            title = "Movie 2",
            averageVote = 7.0,
            moviePosterUrl = "url2",
            totalVotes = 150,
            releaseDate = "2021",
            overview = "Overview 2",
            genreIds = listOf(1, 2, 3),
            order = 2,
        )

    val GENRE_ACTION = GenreResponse(id = 1, name = "Action")
    val GENRE_COMEDY = GenreResponse(id = 2, name = "Comedy")

    val GENRES_RESPONSE =
        GenresResponse(
            genreResponses =
                listOf(
                    GENRE_ACTION,
                    GENRE_COMEDY,
                ),
        )
}
