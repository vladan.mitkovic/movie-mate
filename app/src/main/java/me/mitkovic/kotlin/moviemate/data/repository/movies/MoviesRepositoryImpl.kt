package me.mitkovic.kotlin.moviemate.data.repository.movies

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.common.Constants.UNKNOWN_ERROR_OCCURRED
import me.mitkovic.kotlin.moviemate.data.local.LocalDataSource
import me.mitkovic.kotlin.moviemate.data.local.database.MoviesDatabase
import me.mitkovic.kotlin.moviemate.data.model.MoviesResponse
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.data.paging.MoviesRemoteMediator
import me.mitkovic.kotlin.moviemate.data.paging.SearchMoviesPagingSource
import me.mitkovic.kotlin.moviemate.data.remote.RemoteDataSource
import me.mitkovic.kotlin.moviemate.logging.AppLogger

@OptIn(ExperimentalPagingApi::class)
class MoviesRepositoryImpl(
    private val database: MoviesDatabase,
    private val localDataSource: LocalDataSource,
    private val remoteDataSource: RemoteDataSource,
    private val logger: AppLogger,
) : MoviesRepository {

    private val pageSize = Constants.PAGE_SIZE

    override fun getPopularMovies(onRemoteError: (Throwable) -> Unit): Flow<PagingData<Movie>> =
        Pager(
            config = PagingConfig(pageSize = pageSize),
            remoteMediator =
                MoviesRemoteMediator(
                    database = database,
                    remoteDataSource = remoteDataSource,
                    moviesDataSource =
                        localDataSource
                            .moviesDataSource,
                    logger = logger,
                    onRemoteError = onRemoteError,
                ),
            pagingSourceFactory = {
                localDataSource
                    .moviesDataSource
                    .getMovies()
            },
        ).flow

    override fun getSimilarMovies(movieId: Int): Flow<Resource<MoviesResponse>> =
        flow {
            emit(Resource.Loading())
            val result = remoteDataSource.getSimilarMovies(movieId)
            if (result.isSuccessful) {
                val movies = result.body()?.results ?: emptyList()
                localDataSource
                    .moviesDataSource
                    .saveMovies(movies)
                emit(Resource.Success(MoviesResponse(page = 1, results = movies)))
            } else {
                val errorMessage = result.errorBody()?.string() ?: UNKNOWN_ERROR_OCCURRED
                emit(Resource.Error(message = errorMessage))
            }
        }.catch { e ->
            emit(handleAndLogError(e, logger))
        }

    override suspend fun searchMovies(name: String): Flow<PagingData<Movie>> =
        Pager(
            config =
                PagingConfig(
                    pageSize = 20,
                    enablePlaceholders = false,
                    prefetchDistance = 5,
                ),
            pagingSourceFactory = {
                SearchMoviesPagingSource(
                    remoteDataSource,
                    localDataSource
                        .moviesDataSource,
                    name,
                )
            },
        ).flow

    override fun getMovieDetails(movieId: Int): Flow<Resource<Movie>> =
        flow {
            // Load cached movie details first
            val cachedMovieDetails =
                localDataSource
                    .moviesDataSource
                    .getMovieDetails(movieId)
                    .first()
            emit(Resource.Loading())
            try {
                val response = remoteDataSource.getMovieDetails(movieId)
                if (response.isSuccessful) {
                    val movieDetails =
                        response.body()?.let { newDetails ->
                            // Merge cached details with new details
                            cachedMovieDetails?.let { cached ->
                                newDetails.copy(
                                    genreIds = newDetails.genreIds ?: cached.genreIds,
                                    averageVote = newDetails.averageVote ?: cached.averageVote,
                                    totalVotes = newDetails.totalVotes ?: cached.totalVotes,
                                )
                            } ?: newDetails
                        }

                    // Save merged details to the local database
                    movieDetails?.let {
                        localDataSource
                            .moviesDataSource
                            .saveMovieDetails(it)
                    }

                    emit(Resource.Success(data = movieDetails))
                } else {
                    val errorMessage = response.errorBody()?.string() ?: UNKNOWN_ERROR_OCCURRED
                    emit(handleAndLogError<Movie>(Throwable(errorMessage), logger))
                }
            } catch (e: Exception) {
                emit(handleAndLogError<Movie>(e, logger))
            }
        }.catch { e ->
            emit(handleAndLogError(e, logger))
        }

    override fun getFavoriteMovies(): Flow<Resource<MoviesResponse>> =
        flow {
            emit(Resource.Loading())
            try {
                val favoriteMovies =
                    localDataSource
                        .moviesDataSource
                        .getFavoriteMovies()
                        .first()
                emit(Resource.Success(MoviesResponse(page = 1, results = favoriteMovies)))
            } catch (e: Exception) {
                emit(handleAndLogError(e, logger))
            }
        }

    override suspend fun updateMovieFavoriteStatus(
        movieId: Int,
        favorite: Boolean,
    ) {
        localDataSource
            .moviesDataSource
            .updateMovieFavoriteStatus(movieId, favorite)
    }

    override suspend fun isFavorite(movieId: Int): Boolean =
        localDataSource
            .moviesDataSource
            .getFavoriteMovies()
            .first()
            .any {
                it.id ==
                    movieId
            }

    private fun <T> handleAndLogError(
        e: Throwable,
        logger: AppLogger,
    ): Resource.Error<T> {
        val errorMessage = e.message ?: UNKNOWN_ERROR_OCCURRED
        logger.logError(errorMessage, e)
        return Resource.Error(message = errorMessage)
    }
}
