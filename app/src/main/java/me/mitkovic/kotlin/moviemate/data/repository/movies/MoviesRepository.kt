package me.mitkovic.kotlin.moviemate.data.repository.movies

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.moviemate.data.model.MoviesResponse
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.data.model.database.Movie

interface MoviesRepository {
    fun getPopularMovies(onRemoteError: (Throwable) -> Unit): Flow<PagingData<Movie>>

    fun getMovieDetails(movieId: Int): Flow<Resource<Movie>>

    fun getSimilarMovies(movieId: Int): Flow<Resource<MoviesResponse>>

    suspend fun searchMovies(name: String): Flow<PagingData<Movie>>

    fun getFavoriteMovies(): Flow<Resource<MoviesResponse>>

    suspend fun updateMovieFavoriteStatus(
        movieId: Int,
        favorite: Boolean,
    )

    suspend fun isFavorite(movieId: Int): Boolean
}
