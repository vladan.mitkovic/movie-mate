package me.mitkovic.kotlin.moviemate.ui.navigation

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import me.mitkovic.kotlin.moviemate.ui.screens.details.MovieDetailsScreen
import me.mitkovic.kotlin.moviemate.ui.screens.details.MovieDetailsScreenViewModel
import me.mitkovic.kotlin.moviemate.ui.screens.favorites.FavoritesScreen
import me.mitkovic.kotlin.moviemate.ui.screens.favorites.FavoritesScreenViewModel
import me.mitkovic.kotlin.moviemate.ui.screens.home.MoviesScreen
import me.mitkovic.kotlin.moviemate.ui.screens.home.MoviesScreenViewModel
import me.mitkovic.kotlin.moviemate.ui.screens.settings.SettingsScreen
import me.mitkovic.kotlin.moviemate.ui.screens.settings.SettingsScreenViewModel
import me.mitkovic.kotlin.moviemate.ui.utils.NavigationUtils
import org.koin.androidx.compose.koinViewModel

@Composable
fun MovieMateAppNavigation(
    navHostController: NavHostController,
    paddingValues: PaddingValues,
    onError: (String) -> Unit,
) {
    val onBackClicked: () -> Unit = { navHostController.popBackStack() }

    NavHost(
        navController = navHostController,
        startDestination = Destination.Home,
    ) {
        composable<Destination.Home> {
            val moviesScreenViewModel: MoviesScreenViewModel = koinViewModel()
            MoviesScreen(
                viewModel = moviesScreenViewModel,
                paddingValues = paddingValues,
                onError = onError,
                onMovieClicked = { id ->
                    NavigationUtils.setLastMainRoute(BottomNavigation.HOME.route::class.qualifiedName)
                    navHostController.navigate(Destination.MovieDetails(id))
                },
            )
        }

        composable<Destination.Favorites> {
            val favoritesScreenViewModel: FavoritesScreenViewModel = koinViewModel()
            FavoritesScreen(
                viewModel = favoritesScreenViewModel,
                paddingValues = paddingValues,
                onError = onError,
                onMovieClicked = { id ->
                    NavigationUtils.setLastMainRoute(BottomNavigation.FAVORITES.route::class.qualifiedName)
                    navHostController.navigate(Destination.MovieDetails(id))
                },
                onBackClicked = onBackClicked,
            )
        }

        // Settings
        composable<Destination.Settings> {
            val settingsScreenViewModel: SettingsScreenViewModel = koinViewModel()
            SettingsScreen(
                viewModel = settingsScreenViewModel,
                paddingValues = paddingValues,
                onBackClicked = onBackClicked,
            )
        }

        composable<Destination.MovieDetails> { backStackEntry ->
            val movieDetailsScreenViewModel: MovieDetailsScreenViewModel = koinViewModel()

            MovieDetailsScreen(
                viewModel = movieDetailsScreenViewModel,
                paddingValues = paddingValues,
                onError = onError,
                onSimilarMovieClicked = { id ->
                    navHostController.navigate(Destination.MovieDetails(id))
                },
                onBackClicked = onBackClicked,
            )
        }
    }
}
