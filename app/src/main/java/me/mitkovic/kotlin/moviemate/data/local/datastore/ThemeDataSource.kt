package me.mitkovic.kotlin.moviemate.data.local.datastore

import kotlinx.coroutines.flow.Flow

interface ThemeDataSource {

    suspend fun saveTheme(themeValue: Int)

    fun getTheme(): Flow<Int>
}
