package me.mitkovic.kotlin.moviemate.di

import me.mitkovic.kotlin.moviemate.data.local.LocalDataSource
import me.mitkovic.kotlin.moviemate.data.local.database.MoviesDatabase
import me.mitkovic.kotlin.moviemate.data.remote.RemoteDataSource
import me.mitkovic.kotlin.moviemate.data.repository.MovieMateRepository
import me.mitkovic.kotlin.moviemate.data.repository.MovieMateRepositoryImpl
import me.mitkovic.kotlin.moviemate.data.repository.genres.GenresRepository
import me.mitkovic.kotlin.moviemate.data.repository.genres.GenresRepositoryImpl
import me.mitkovic.kotlin.moviemate.data.repository.movies.MoviesRepository
import me.mitkovic.kotlin.moviemate.data.repository.movies.MoviesRepositoryImpl
import me.mitkovic.kotlin.moviemate.data.repository.theme.ThemeRepository
import me.mitkovic.kotlin.moviemate.data.repository.theme.ThemeRepositoryImpl
import me.mitkovic.kotlin.moviemate.logging.AppLogger
import org.koin.dsl.module

val repositoryModule =
    module {
        single<MoviesRepository> {
            MoviesRepositoryImpl(
                database = get<MoviesDatabase>(),
                localDataSource = get<LocalDataSource>(),
                remoteDataSource = get<RemoteDataSource>(),
                logger = get<AppLogger>(),
            )
        }

        single<GenresRepository> {
            GenresRepositoryImpl(
                localDataSource = get<LocalDataSource>(),
                remoteDataSource = get<RemoteDataSource>(),
                logger = get<AppLogger>(),
            )
        }

        single<ThemeRepository> {
            ThemeRepositoryImpl(
                localDataSource = get<LocalDataSource>(),
            )
        }

        single<MovieMateRepository> {
            MovieMateRepositoryImpl(
                moviesRepository = get<MoviesRepository>(),
                genresRepository = get<GenresRepository>(),
                themeRepository = get<ThemeRepository>(),
            )
        }
    }
