package me.mitkovic.kotlin.moviemate.testing.data.local.database

import androidx.paging.PagingSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update
import me.mitkovic.kotlin.moviemate.data.local.database.movies.MoviesDataSource
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.testing.data.common.TestPagingSource
import kotlin.collections.plus

object TestMoviesDataSourceImpl : MoviesDataSource {

    private val moviesFlow = MutableStateFlow<List<Movie>>(emptyList())

    override suspend fun saveMovies(movies: List<Movie>) {
        moviesFlow.update { currentMovies ->
            currentMovies + movies
        }
    }

    override fun getAllMovies(): Flow<List<Movie>> = moviesFlow

    override fun getMovies(): PagingSource<Int, Movie> = TestPagingSource(moviesFlow.value)

    override suspend fun saveMovieDetails(movieDetails: Movie) {
        moviesFlow.update { currentMovies ->
            val index = currentMovies.indexOfFirst { it.id == movieDetails.id }
            if (index != -1) {
                currentMovies.toMutableList().apply {
                    this[index] = movieDetails
                }
            } else {
                currentMovies + movieDetails
            }
        }
    }

    override fun getMovieDetails(movieId: Int): Flow<Movie?> =
        moviesFlow.map {
            it.find { movie ->
                movie.id ==
                    movieId
            }
        }

    override fun searchMovies(name: String): Flow<List<Movie>> =
        moviesFlow.map { currentMovies ->
            currentMovies.filter { it.title?.contains(name, ignoreCase = true) == true }
        }

    override suspend fun deleteAllMovies() {
        moviesFlow.value = emptyList()
    }

    override fun getFavoriteMovies(): Flow<List<Movie>> =
        moviesFlow.map { currentMovies ->
            currentMovies.filter { it.isFavorite }
        }

    override suspend fun updateMovieFavoriteStatus(
        movieId: Int,
        isFavorite: Boolean,
    ) {
        moviesFlow.update { currentMovies ->
            currentMovies.map { movie ->
                if (movie.id == movieId) movie.copy(isFavorite = isFavorite) else movie
            }
        }
    }
}
