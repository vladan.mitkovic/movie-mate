package me.mitkovic.kotlin.moviemate.testing.data.repository.genres

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import me.mitkovic.kotlin.moviemate.data.model.GenreResponse
import me.mitkovic.kotlin.moviemate.data.model.GenresResponse
import me.mitkovic.kotlin.moviemate.data.model.Resource
import me.mitkovic.kotlin.moviemate.data.repository.genres.GenresRepository

object TestGenresRepositoryImpl : GenresRepository {

    private var genres: List<GenreResponse> = emptyList()

    fun setGenres(genres: List<GenreResponse>) {
        this.genres = genres
    }

    override fun getGenres(): Flow<Resource<GenresResponse>> =
        flow {
            emit(Resource.Loading())
            if (genres.isNotEmpty()) {
                emit(Resource.Success(GenresResponse(genres)))
            } else {
                emit(Resource.Error("No genres available"))
            }
        }

    override suspend fun getGenreById(genreId: Int): GenreResponse? = genres.find { it.id == genreId }
}
