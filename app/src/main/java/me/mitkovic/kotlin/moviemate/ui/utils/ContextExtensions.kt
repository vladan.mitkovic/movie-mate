package me.mitkovic.kotlin.moviemate.ui.utils

import android.content.Context
import me.mitkovic.kotlin.moviemate.R

fun Context.getAppVersionName(): String {
    val packageInfo =
        try {
            packageManager.getPackageInfo(packageName, 0)
        } catch (e: Exception) {
            null
        }
    return packageInfo?.versionName ?: getString(R.string.app_version_unknown)
}

fun Context.getAppVersionCode(): Long =
    try {
        val packageInfo = packageManager.getPackageInfo(packageName, 0)
        when {
            android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P -> packageInfo.longVersionCode
            else ->
                @Suppress("DEPRECATION")
                packageInfo.versionCode.toLong()
        }
    } catch (e: Exception) {
        -1L
    }
