package me.mitkovic.kotlin.moviemate.ui.navigation

import kotlinx.serialization.Serializable

sealed class Destination {
    @Serializable
    object Home : Destination()

    @Serializable
    object Favorites : Destination()

    @Serializable
    object Settings : Destination()

    @Serializable
    data class MovieDetails(
        val id: Int,
    ) : Destination()
}
