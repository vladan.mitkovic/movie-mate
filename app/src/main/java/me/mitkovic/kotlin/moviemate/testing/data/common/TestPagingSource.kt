package me.mitkovic.kotlin.moviemate.testing.data.common

import androidx.paging.PagingSource
import androidx.paging.PagingState
import me.mitkovic.kotlin.moviemate.data.model.database.Movie

class TestPagingSource(
    private val movies: List<Movie>,
) : PagingSource<Int, Movie>() {

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> =
        try {
            LoadResult.Page(
                data = movies,
                prevKey = null,
                nextKey = null,
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
}
