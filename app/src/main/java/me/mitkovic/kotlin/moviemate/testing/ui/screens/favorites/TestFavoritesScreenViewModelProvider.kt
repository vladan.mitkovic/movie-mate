package me.mitkovic.kotlin.moviemate.testing.ui.screens.favorites

import me.mitkovic.kotlin.moviemate.testing.common.TestAppLoggerImpl
import me.mitkovic.kotlin.moviemate.testing.data.repository.TestMovieMateRepositoryImpl
import me.mitkovic.kotlin.moviemate.ui.screens.favorites.FavoritesScreenViewModel

object TestFavoritesScreenViewModelProvider {

    fun provideFavoritesScreenViewModel(): FavoritesScreenViewModel =
        FavoritesScreenViewModel(
            movieMateRepository = TestMovieMateRepositoryImpl,
            logger = TestAppLoggerImpl,
        )
}
