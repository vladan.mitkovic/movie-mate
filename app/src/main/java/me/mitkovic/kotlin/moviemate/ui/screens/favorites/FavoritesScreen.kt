package me.mitkovic.kotlin.moviemate.ui.screens.favorites

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.ArrowBack
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import me.mitkovic.kotlin.moviemate.R
import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.testing.data.repository.movies.TestMoviesRepositoryImpl
import me.mitkovic.kotlin.moviemate.testing.ui.screens.favorites.TestFavoritesScreenViewModelProvider
import me.mitkovic.kotlin.moviemate.ui.common.DarkModePreview
import me.mitkovic.kotlin.moviemate.ui.common.LightModePreview
import me.mitkovic.kotlin.moviemate.ui.screens.home.MovieItem
import me.mitkovic.kotlin.moviemate.ui.theme.fontSizes
import me.mitkovic.kotlin.moviemate.ui.theme.spacing
import me.mitkovic.kotlin.moviemate.ui.utils.DevicePreviews
import org.koin.androidx.compose.koinViewModel

@Composable
fun FavoritesScreen(
    viewModel: FavoritesScreenViewModel = koinViewModel(),
    paddingValues: PaddingValues,
    onError: (String) -> Unit,
    onMovieClicked: (Int) -> Unit,
    onBackClicked: () -> Unit,
) {
    val spacing = MaterialTheme.spacing
    val favoriteMoviesUiState by viewModel.favoriteMoviesUiState.collectAsStateWithLifecycle()
    var isRefreshing by remember { mutableStateOf(false) }

    LaunchedEffect(favoriteMoviesUiState.isLoading) {
        isRefreshing = favoriteMoviesUiState.isLoading
    }

    // If error happens, post it up the tree until the MainActivity
    // And handle it into Scaffold SnackbarHost
    LaunchedEffect(key1 = favoriteMoviesUiState.error) {
        favoriteMoviesUiState.error?.let { message ->
            onError(message)
        }
    }

    Column(
        modifier =
            Modifier
                .padding(paddingValues)
                .background(MaterialTheme.colorScheme.background),
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier =
                Modifier
                    .fillMaxWidth()
                    .padding(spacing.small),
        ) {
            IconButton(
                onClick = { onBackClicked() },
                modifier = Modifier,
            ) {
                Icon(
                    imageVector = Icons.AutoMirrored.Rounded.ArrowBack,
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.onBackground,
                    modifier = Modifier.size(spacing.xLarge),
                )
            }
            Text(
                stringResource(R.string.favorites),
                modifier = Modifier.padding(start = spacing.small),
                style = MaterialTheme.typography.headlineLarge,
                color = MaterialTheme.colorScheme.onBackground,
            )
        }

        when {
            favoriteMoviesUiState.isLoading -> {
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center,
                ) {
                    CircularProgressIndicator(color = MaterialTheme.colorScheme.primary)
                }
            }
            favoriteMoviesUiState.favoriteMovies.favoriteMovies.isNotEmpty() -> {
                LazyVerticalGrid(
                    columns = GridCells.Fixed(2),
                    modifier = Modifier.padding(start = spacing.small, end = spacing.small),
                ) {
                    items(favoriteMoviesUiState.favoriteMovies.favoriteMovies) { movie ->
                        MovieItem(movie = movie, onClick = { onMovieClicked(movie.id) })
                    }
                }
            }
            else -> {
                val fontSizes = MaterialTheme.fontSizes
                Text(
                    text = stringResource(R.string.no_favorite_movie_found),
                    modifier =
                        Modifier
                            .align(Alignment.CenterHorizontally)
                            .padding(top = spacing.medium, start = spacing.medium),
                    color = MaterialTheme.colorScheme.onBackground,
                    fontSize = fontSizes.medium,
                )
            }
        }
    }
}

@Composable
@DevicePreviews
fun FavoritesScreenDarkModePreview() {
    DarkModePreview {
        // Use fake data
        val moviesDetailsList =
            listOf(
                Constants.MOVIE1,
                Constants.MOVIE2,
            )

        // Set fake movies in the repository
        TestMoviesRepositoryImpl.setPopularMovies(moviesDetailsList)

        // Provide the fake ViewModel
        val viewModel = TestFavoritesScreenViewModelProvider.provideFavoritesScreenViewModel()

        // Display the FavoritesScreen with the fake data
        FavoritesScreen(
            viewModel = viewModel,
            paddingValues = PaddingValues(),
            onError = {},
            onMovieClicked = {},
            onBackClicked = {},
        )
    }
}

@Composable
@DevicePreviews
fun FavoritesScreenLightModePreview() {
    LightModePreview {
        // Use fake data
        val moviesDetailsList =
            listOf(
                Constants.MOVIE1,
                Constants.MOVIE2,
            )

        // Set fake movies in the repository
        TestMoviesRepositoryImpl.setPopularMovies(moviesDetailsList)

        // Provide the fake ViewModel
        val viewModel = TestFavoritesScreenViewModelProvider.provideFavoritesScreenViewModel()

        // Display the FavoritesScreen with the fake data
        FavoritesScreen(
            viewModel = viewModel,
            paddingValues = PaddingValues(),
            onError = {},
            onMovieClicked = {},
            onBackClicked = {},
        )
    }
}
