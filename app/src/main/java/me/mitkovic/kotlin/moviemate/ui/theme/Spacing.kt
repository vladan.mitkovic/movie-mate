package me.mitkovic.kotlin.moviemate.ui.theme

import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

data class Spacing(
    val extraSmall: Dp = 4.dp,
    val small: Dp = 8.dp,
    val extraMedium: Dp = 12.dp,
    val medium: Dp = 16.dp,
    val large: Dp = 24.dp,
    val xLarge: Dp = 64.dp,
    val xxLarge: Dp = 96.dp,
    val xxxLarge: Dp = 128.dp,
    val dividerWidth: Dp = 1.dp,
    val genreDotSize: Dp = 6.dp,
    val moviePosterCornerRadius: Dp = 14.dp,
    val movieItemBottomPadding: Dp = 20.dp,
    val searchIconSize: Dp = 24.dp,
    val favoriteIconSize: Dp = 32.dp,
    val searchFieldHeight: Dp = 36.dp,
    val moviePosterWidth: Dp = 200.dp,
    val moviePosterHeight: Dp = 300.dp,
)

data class FontSizes(
    val small: TextUnit = 14.sp,
    val medium: TextUnit = 18.sp,
    val large: TextUnit = 24.sp,
)

val LocalSpacing = compositionLocalOf { Spacing() }
val LocalFontSizes = compositionLocalOf { FontSizes() }

val MaterialTheme.spacing: Spacing
    @Composable
    @ReadOnlyComposable
    get() = LocalSpacing.current

val MaterialTheme.fontSizes: FontSizes
    @Composable
    @ReadOnlyComposable
    get() = LocalFontSizes.current
