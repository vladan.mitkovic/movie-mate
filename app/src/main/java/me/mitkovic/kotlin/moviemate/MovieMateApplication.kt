package me.mitkovic.kotlin.moviemate

import android.app.Application
import me.mitkovic.kotlin.moviemate.di.dataModule
import me.mitkovic.kotlin.moviemate.di.networkModule
import me.mitkovic.kotlin.moviemate.di.repositoryModule
import me.mitkovic.kotlin.moviemate.di.utilityModule
import me.mitkovic.kotlin.moviemate.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class MovieMateApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidContext(this@MovieMateApplication)
            modules(
                listOf(
                    dataModule,
                    networkModule,
                    repositoryModule,
                    viewModelModule,
                    utilityModule,
                ),
            )
        }
    }
}
