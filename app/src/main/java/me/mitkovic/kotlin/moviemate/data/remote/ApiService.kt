package me.mitkovic.kotlin.moviemate.data.remote

import me.mitkovic.kotlin.moviemate.common.Constants
import me.mitkovic.kotlin.moviemate.data.model.GenresResponse
import me.mitkovic.kotlin.moviemate.data.model.MoviesResponse
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    // popular movies
    @GET("movie/popular")
    suspend fun getPopularMovies(
        @Query("page") page: Int,
        @Query("api_key") apiKey: String = Constants.API_KEY,
    ): Response<MoviesResponse>

    // movie details
    @GET("movie/{movie_id}")
    suspend fun getMovieDetails(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String = Constants.API_KEY,
    ): Response<Movie>

    // similar movies
    @GET("movie/{movie_id}/similar")
    suspend fun getSimilarMovies(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String = Constants.API_KEY,
    ): Response<MoviesResponse>

    /** searches movies */
    @GET("search/movie")
    suspend fun searchMovie(
        @Query("query") movieName: String,
        @Query("page") page: Int,
        @Query("api_key") apiKey: String = Constants.API_KEY,
    ): Response<MoviesResponse>

    @GET("genre/movie/list")
    suspend fun getMovieGenres(
        @Query("api_key") apiKey: String = Constants.API_KEY,
        @Query("language") language: String = "en",
    ): Response<GenresResponse>
}
