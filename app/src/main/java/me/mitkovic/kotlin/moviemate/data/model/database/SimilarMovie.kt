package me.mitkovic.kotlin.moviemate.data.model.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "similar_movies")
data class SimilarMovie(
    @PrimaryKey val id: Int,
    val movieId: Int,
    val title: String?,
    val averageVote: Double?,
    val moviePosterUrl: String?,
    val totalVotes: Int?,
    val releaseDate: String?,
    val overview: String?,
    val order: Int
)
