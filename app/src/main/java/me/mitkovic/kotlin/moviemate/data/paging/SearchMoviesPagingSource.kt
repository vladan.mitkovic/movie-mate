package me.mitkovic.kotlin.moviemate.data.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import kotlinx.coroutines.flow.first
import me.mitkovic.kotlin.moviemate.data.local.database.movies.MoviesDataSource
import me.mitkovic.kotlin.moviemate.data.model.MoviesResponse
import me.mitkovic.kotlin.moviemate.data.model.database.Movie
import me.mitkovic.kotlin.moviemate.data.remote.RemoteDataSource
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

class SearchMoviesPagingSource(
    private val remoteDataSource: RemoteDataSource,
    private val moviesDataSource: MoviesDataSource,
    private val searchQuery: String,
) : PagingSource<Int, Movie>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        val nextPage = params.key ?: 1

        return try {
            val cachedMovies = getCachedMovies(nextPage)
            if (cachedMovies.isNotEmpty()) {
                return LoadResult.Page(
                    data = cachedMovies,
                    prevKey = null,
                    nextKey = nextPage + 1,
                )
            }

            val response = getRemoteMovies(nextPage)
            if (response.isSuccessful) {
                val movies = response.body()?.results ?: emptyList()
                moviesDataSource.saveMovies(movies)

                LoadResult.Page(
                    data = movies,
                    prevKey = if (nextPage == 1) null else nextPage - 1,
                    nextKey = if (movies.isEmpty()) null else nextPage + 1,
                )
            } else {
                LoadResult.Error(HttpException(response))
            }
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? = state.anchorPosition

    suspend fun getCachedMovies(nextPage: Int): List<Movie> =
        if (nextPage == 1) {
            moviesDataSource.searchMovies(searchQuery).first()
        } else {
            emptyList()
        }

    suspend fun getRemoteMovies(nextPage: Int): Response<MoviesResponse> = remoteDataSource.searchMovie(searchQuery, nextPage)
}
