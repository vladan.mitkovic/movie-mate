package me.mitkovic.kotlin.moviemate.data.repository

import me.mitkovic.kotlin.moviemate.data.repository.genres.GenresRepository
import me.mitkovic.kotlin.moviemate.data.repository.movies.MoviesRepository
import me.mitkovic.kotlin.moviemate.data.repository.theme.ThemeRepository

interface MovieMateRepository {
    val moviesRepository: MoviesRepository
    val genresRepository: GenresRepository
    val themeRepository: ThemeRepository
}
