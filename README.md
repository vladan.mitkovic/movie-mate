# Movie Mate

Movie Mate is simple two screens app showing modern way of creating Android applications.

It's using themoviedb API to fetch popular movies:
https://developer.themoviedb.org/docs/getting-started

## Features

- Fetching popular movies
- Searching moves
- Movie detail with movie details and similar movies
- Favorites
- Offline mode: Al listed and visited movies are also available offline
- User settings

### Screenshots

<div style="display: flex;">
    <img src="previews/Screenshot_1_mmate.jpg" alt="Screenshot 1" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_2_mmate.jpg" alt="Screenshot 2" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_3_mmate.jpg" alt="Screenshot 3" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_4_mmate.jpg" alt="Screenshot 4" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_5_mmate.jpg" alt="Screenshot 4" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_6_mmate.jpg" alt="Screenshot 4" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_7_mmate.jpg" alt="Screenshot 4" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_8_mmate.jpg" alt="Screenshot 4" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_9_mmate.jpg" alt="Screenshot 4" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_10_mmate.jpg" alt="Screenshot 4" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_11_mmate.jpg" alt="Screenshot 4" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_12_mmate.jpg" alt="Screenshot 4" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_13_mmate.jpg" alt="Screenshot 4" width="250">
&nbsp; &nbsp; &nbsp; &nbsp;
    <img src="previews/Screenshot_14_mmate.jpg" alt="Screenshot 4" width="250">
</div>

## Tech Stack

Movie Mate is built using a modern stack of technologies designed to offer robust performance and scalability:
- **Kotlin**: For almost everything.
- **Jetpack Compose**: For building native UIs.
- **Retrofit**: For network operations.
- **Room Database**: For local data storage.
- **DataStore**: For local data storage.
- **MVVM (Model-View-ViewModel)**: Architectural pattern.
- **Koin**: Dependency injection.
- **Paging**: For using network bandwidth and system resources efficiently.
- **Navigation**: For managing in-app navigation.
- **Testing**: Robust testing with JUnit.
- **Material 3**: Latest Material Design components.

## Architecture

Movie Mate follows the [Google's official architecture guidance](https://developer.android.com/topic/architecture).
<br/><br/>
<img src="previews/mmate_1_Architecture.png" alt="Architecture 1">
<br/><br/>
Movie Mate was built with [Guide to app architecture](https://developer.android.com/topic/architecture), so it would be a great sample to show how the architecture works in real-world projects.
<br/><br/>
The overall architecture is composed of two layers; UI Layer and the data layer. Each layer has dedicated components and they each have different responsibilities. The arrow means the component has a dependency on the target component following its direction.
<br/><br/>

### Architecture Overview
<br/><br/>
<img src="previews/mmate_2_Architecture.png" alt="Architecture 2" width="600">
<br/><br/>
Each layer has different responsibilities below. Basically, they follow [unidirectional event/data flow](https://developer.android.com/topic/architecture/ui-layer#udf).
<br/><br/>

### UI Layer
<br/><br/>
<img src="previews/mmate_3_Architecture.png" alt="Architecture 3" width="600">
<br/><br/>
The UI Layer consists of UI elements like buttons, menus, tabs that could interact with users and [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) that holds app states and restores data when configuration changes.
<br/><br/>

### Data Layer
<br/><br/>
<img src="previews/mmate_4_Architecture.png" alt="Architecture 4" width="600">
<br/><br/>
The data Layer consists of repositories, which include business logic, such as querying data from the local data store and requesting remote data from the network. It is implemented as an offline-first source of business logic and follows the [single source of truth](https://en.wikipedia.org/wiki/Single_source_of_truth) principle.

### License
Designed and developed by Vladan Mitkovic (2024)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.